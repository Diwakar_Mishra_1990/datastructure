package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckWhetherArrayCanBeDividedinPairsWithSumK {

	public static void main(String[] args) {
		Integer lInputArray[] = new Integer[] { 9, 7, 5, 3 };
		int k = 6;
		Integer[] porcessedArr = new Integer[lInputArray.length];
        processeInputs(porcessedArr,lInputArray,k);
        Map<Integer,Integer> lHashmap=new HashMap<Integer,Integer>();
        List<Integer> lComboList=createHashMap(lHashmap,porcessedArr,lInputArray,k);
        printArray(lInputArray);
        printArray(porcessedArr);
        System.out.println(lHashmap);
        System.out.println(lComboList);
        if(lComboList.size()!=lInputArray.length)
        {
        	System.out.println("PAIRS NOT FOUND");
        }
        else
        {
        	System.out.println("PAIRS FOUND");
        	System.out.println("THE PAIRS ARE");
        	for(int i=0;i<lComboList.size();i++)
        	{
        		System.out.println(lComboList.get(i)+","+lComboList.get(++i));
        	}
        }
	}

	private static void printArray(Object[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
			for (int j = 0; j < lDynamicArrayMap.length; j++) {
				System.out.print(lDynamicArrayMap[j].toString() + "\t");
			}
			System.out.println("");
	}
	private static List<Integer> createHashMap(Map<Integer, Integer> lHashmap, Integer[] porcessedArr, Integer[] lInputArray, int k) {
		// TODO Auto-generated method stub
		List<Integer> lComboList= new ArrayList<Integer>();
		for(int i=0;i<porcessedArr.length;i++)
		{
			if(lHashmap.containsKey(porcessedArr[i]))
			{
				lComboList.add(lInputArray[i]);
				lComboList.add(lHashmap.get(porcessedArr[i]));
			}
			lHashmap.put(k-porcessedArr[i],lInputArray[i] );
		}
		return lComboList;
	}

	private static void processeInputs(Integer[] processedInputArray, Integer[] inputArray, int k) {
		// TODO Auto-generated method stub
		for(int i=0;i<inputArray.length;i++)
		{
			processedInputArray[i]=inputArray[i]%k;
		}
	}
}
