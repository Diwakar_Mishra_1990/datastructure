package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

public class ItenaryFromGivenTicket {
	public static void main(String[] args) {
		String inputs = "CHENNAI,BANGALORE\nBOMBAY,DELHI\nGOA,CHENNAI\nDELHI,GOA";
		List<String> lFromList = new ArrayList<String>();
		List<String> toList = new ArrayList<String>();
		List<String> lTotalList = new ArrayList<String>();
		Map<String, String> lAdjacencyList = createFromToPoints(inputs,
				lFromList, toList, lTotalList);
		List<String> lItenary = createItenary(lAdjacencyList, lTotalList,
				toList, lFromList);
		System.out.println(lItenary);
	}

	private static List<String> createItenary(
			Map<String, String> lAdjacencyList, List<String> lTotalList,
			List<String> toList, List<String> lFromList) {
		List<String> lItenaryList=new ArrayList<String>();
		List<String> lTempTotalList=new ArrayList<String>();
		lTempTotalList.addAll(lTotalList);
		lTempTotalList.removeAll(toList);
		String Source = lTempTotalList.get(0);
		lTempTotalList.remove(0);
		lTempTotalList.addAll(lTotalList);
		lTempTotalList.removeAll(lFromList);
		String destination=lTempTotalList.get(0);
		String next=Source;
		while (!next.equals(destination))
		{
			lItenaryList.add(next);
			next=lAdjacencyList.get(next);
		}
		lItenaryList.add(next);
		return lItenaryList;
	}

	public static Map<String, String> createFromToPoints(String edgeList,
			List<String> lFromList, List<String> toList, List<String> lTotalList) {
		// TODO Auto-generated method stub
		Map<String, String> edgeListMap = new HashMap<String, String>();
		String[] edges = edgeList.split("\n");
		for (int i = 0; i < edges.length; i++) {
			String[] edgeVertices = edges[i].split(",");
			addInMap(edgeListMap, edgeVertices[0], edgeVertices[1]);
			addInList(lFromList, edgeVertices[0]);
			addInList(toList, edgeVertices[1]);
			addInList(lTotalList, edgeVertices[0]);
			addInList(lTotalList, edgeVertices[1]);
		}
		return edgeListMap;
	}

	private static void addInList(List<String> lList, String edgeVertices) {
		if (!lList.contains(edgeVertices)) {
			lList.add(edgeVertices);
		}
	}

	private static void addInMap(Map<String, String> edgeListMap,
			String incidentedgeVertices, String targetEdgeVertices) {
		edgeListMap.put(incidentedgeVertices, targetEdgeVertices);
	}
}
