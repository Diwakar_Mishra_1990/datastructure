package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LargestSubArrayWith0Sum {

	public static void main(String[] args) {
		Integer sum = 0;
		Integer[] inputArray = new Integer[] {15, -2, 2, -8, 1, 7, 10, 23};
		Map<Integer, List<Integer>> compositeMap = new HashMap<Integer, List<Integer>>();
		checkSumCombination(sum, inputArray ,compositeMap);
		System.out.println(compositeMap);
	}

	private static void checkSumCombination(Integer sum, Integer[] inputArray,
			 Map<Integer, List<Integer>> compositeMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < inputArray.length; i++) {
			int key=sum - inputArray[i];
			addInMap(compositeMap,key,inputArray[i]);
		}
	}

	private static void addInMap(Map<Integer, List<Integer>> edgeListMap, Integer lKey, Integer lValues) {
		if (edgeListMap.keySet().contains(lKey)) {
			edgeListMap.get(lKey).add(lValues);
		} else {
			List<Integer> edgeListList = new ArrayList<Integer>();
			edgeListList.add(lValues);
			edgeListMap.put(lKey, edgeListList);
		}
	}
	private static void printArray(Object[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int j = 0; j < lDynamicArrayMap.length; j++) {
			System.out.print(lDynamicArrayMap[j].toString() + "\t");
		}
		System.out.println("");
	}
}
