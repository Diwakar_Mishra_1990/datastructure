package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FindPairWithGivenSum {

	public static void main(String[] args) {
		Integer sum = 6;
		Integer[] inputArray = new Integer[] { 1, 2, 3, 4, 3, 2, 1, 4 };
		Map<Object, Integer> compositeMap = new HashMap<Object, Integer>();
		Integer[] processedInputArray = new Integer[inputArray.length];
		Map<Object, Integer> processedInout = processInputForDuplicate(
				inputArray, processedInputArray, compositeMap);
		checkSumCombination(sum, inputArray, processedInputArray, compositeMap);
	}

	private static void checkSumCombination(Integer sum, Integer[] inputArray,
			Integer[] processedInputArray, Map<Object, Integer> compositeMap) {
		// TODO Auto-generated method stub
		Map<Integer, Object> processedInout = new HashMap<Integer, Object>();
		for (int i = 0; i < processedInputArray.length; i++) {
			if (processedInout.containsKey(processedInputArray[i])) {
				System.out.println("Following combinations make sum " + sum);
				System.out.println(compositeMap.get(processedInout
						.get(processedInputArray[i]))
						+ "*"
						+ processedInout.get(processedInputArray[i])
						+ " and "
						+ compositeMap.get(processedInputArray[i])
						+ "*"
						+ processedInputArray[i]);
			}
			processedInout.put(sum - processedInputArray[i],
					processedInputArray[i]);
		}
	}

	private static Map<Object, Integer> processInputForDuplicate(
			Integer[] inputArray, Integer[] processedInputArray,
			Map<Object, Integer> compositeMap) {
		// TODO Auto-generated method stub

		Map<Object, Integer> lInputList = new HashMap<Object, Integer>();
		for (int i = 0; i < inputArray.length; i++) {
			if (lInputList.containsKey(inputArray[i])) {
				int val = lInputList.get(inputArray[i]);
				val++;
				lInputList.put(inputArray[i], val);
				processedInputArray[i] = val * inputArray[i];
				compositeMap.put(val * inputArray[i], inputArray[i]);
				continue;
			}
			compositeMap.put(i, inputArray[i]);
			lInputList.put(inputArray[i], 1);
			processedInputArray[i] = inputArray[i];
		}
		return null;
	}

	private static void printArray(Object[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int j = 0; j < lDynamicArrayMap.length; j++) {
			System.out.print(lDynamicArrayMap[j].toString() + "\t");
		}
		System.out.println("");
	}
}
