package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TraversingTreeInVerticalOrder {

	public static void main(String[] args) {
		String []inputs=new String[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};
		Integer []inputHash=new Integer[inputs.length];
		Map<Integer,List<Object>> lHashMap=new TreeMap<Integer,List<Object>>();
		traverseVertically(inputs,lHashMap,inputHash);
		printArray(inputs);
		printArray(inputHash);
		System.out.println(lHashMap);
		traversingVerticallyFromResult(lHashMap);
	}

	private static void traversingVerticallyFromResult(
			Map<Integer, List<Object>> lHashMap) {
		// TODO Auto-generated method stub
		for(Integer index:lHashMap.keySet())
		{
			System.out.println("Nodes AT Index "+index+" are as follows");
			System.out.println(lHashMap.get(index));
		}
	}

	private static void traverseVertically(Object[] inputs,
			Map<Integer, List<Object>> lHashMap, Integer[] inputHash) {
		// TODO Auto-generated method stub
		int count=0;
		inputHash[0]=0;
		//lHashMap.put(key, value)
		for(int i=0;i<inputs.length;i++)
		{
			if(inputHash[i]!=null && 2*i+1<inputHash.length)
			{
				inputHash[2*i+1]=inputHash[i]-1;
				if(2*i+2<inputHash.length)
				{
					inputHash[2*i+2]=inputHash[i]+1;
				}
			}
		}
		for(int i=0;i<inputs.length;i++)
		{
			addinMap(inputs, lHashMap,inputHash ,i);
		}
	}

	private static void addinMap(Object[] inputs,
			Map<Integer, List<Object>> lHashMap, Integer[] inputHash, int index) {
		// TODO Auto-generated method stub

		if(!lHashMap.containsKey(inputHash[index]))
		{
			List<Object> lHashMapList= new ArrayList<Object>();
			lHashMapList.add(inputs[index]);
			lHashMap.put(inputHash[index], lHashMapList);
		}
		else
		{
			lHashMap.get(inputHash[index]).add(inputs[index]);
		}
	
	}

	private static void printArray(Object[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
			for (int j = 0; j < lDynamicArrayMap.length; j++) {
				System.out.print(lDynamicArrayMap[j].toString() + "\t");
			}
			System.out.println("");
	}
}
