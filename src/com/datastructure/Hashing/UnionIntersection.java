package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 1021322 The class is modified for linkedList but uses array howevwer
 *         intrinsicFunction may change
 */
public class UnionIntersection {

	public static void main(String[] args) {
		Object input1[] = new Integer[] { 10, 15, 4, 20 };
		Object input2[] = new Integer[] { 8, 4, 2, 10 };
		Map<Object, Integer> lHashMap1 = cerateHashMap(input1);
		Map<Object, Integer> lHashMap2 = cerateHashMap(input2);
		List<Object> lIntersectionList = getIntersectionList(lHashMap1,
				lHashMap2);
		List<Object> lUnionList = getUnionList(lHashMap1, lHashMap2);
		printArray(input1);
		printArray(input2);
		System.out.println(lIntersectionList);
		System.out.println(lUnionList);
	}
	private static void printArray(Object[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
			for (int j = 0; j < lDynamicArrayMap.length; j++) {
				System.out.print(lDynamicArrayMap[j].toString() + "\t");
			}
			System.out.println("");
	}

	private static List<Object> getUnionList(Map<Object, Integer> lHashMap1,
			Map<Object, Integer> lHashMap2) {
		// TODO Auto-generated method stub
		Map<Object, Integer> lHashMap = new HashMap<Object, Integer>();
		Map<Object, Integer> lHashMapTemp = new HashMap<Object, Integer>();
		Map<Object, Integer> lHashUniontemp = new HashMap<Object, Integer>();
		List<Object> lUnionList = new ArrayList<Object>();
		//not required piece of code . just copy paste issue
		if (lHashMap1.keySet().size() < lHashMap2.keySet().size()) {
			lHashMap = lHashMap1;
			lHashMapTemp = lHashMap2;
		} else {
			lHashMap = lHashMap2;
			lHashMapTemp = lHashMap1;
		}
		for (Object o : lHashMap.keySet()) {
			if (lHashMapTemp.containsKey(o)) {
				int num = Math.max(lHashMapTemp.get(o), lHashMap.get(o));
				lHashUniontemp.put(o, num);
				continue;
			}
			lHashUniontemp.put(o, lHashMap.get(o));
		}
		for (Object o : lHashMapTemp.keySet()) {
			if (lHashMap.containsKey(o)) {
				int num = Math.max(lHashMapTemp.get(o), lHashMap.get(o));
				lHashUniontemp.put(o, num);
				continue;
			}
			lHashUniontemp.put(o, lHashMapTemp.get(o));
		}
		for(Object o :lHashUniontemp.keySet())
		{
			int count=lHashUniontemp.get(o);
			for(int i=0;i<count;i++)
			{
				lUnionList.add(o);
			}
		}

		return lUnionList;
	}

	private static List<Object> getIntersectionList(
			Map<Object, Integer> lHashMap1, Map<Object, Integer> lHashMap2) {
		// TODO Auto-generated method stub
		Map<Object, Integer> lHashMap = new HashMap<Object, Integer>();
		Map<Object, Integer> lHashMapTemp = new HashMap<Object, Integer>();
		Map<Object, Integer> lHashIntersectionemp = new HashMap<Object, Integer>();
		List<Object> lIntersectionList = new ArrayList<Object>();
		if (lHashMap1.keySet().size() < lHashMap2.keySet().size()) {
			lHashMap = lHashMap1;
			lHashMapTemp = lHashMap2;
		} else {
			lHashMap = lHashMap2;
			lHashMapTemp = lHashMap1;
		}
		for (Object o : lHashMap.keySet()) {
			if (lHashMapTemp.containsKey(o)) {
				int num = Math.min(lHashMapTemp.get(o), lHashMap.get(o));
				lHashIntersectionemp.put(o, num);

			}
		}
		for(Object o :lHashIntersectionemp.keySet())
		{
			int count=lHashIntersectionemp.get(o);
			for(int i=0;i<count;i++)
			{
				lIntersectionList.add(o);
			}
		}

		return lIntersectionList;
	}

	private static Map<Object, Integer> cerateHashMap(Object[] input) {
		// TODO Auto-generated method stub
		Map<Object, Integer> lHashMap = new HashMap<Object, Integer>();
		for (int index = 0; index < input.length; index++) {
			if (!lHashMap.containsKey(input[index])) {
				lHashMap.put(input[index], 1);
			} else {
				int value = lHashMap.get(input[index]);
				value++;
				lHashMap.put(input[index], value);
			}
		}
		return lHashMap;
	}
}
