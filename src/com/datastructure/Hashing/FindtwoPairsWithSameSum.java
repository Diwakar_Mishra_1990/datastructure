package com.datastructure.Hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Diwakar Mishra Find four elements a, b, c and d in an array such that
 *         a+b = c+d
 */
public class FindtwoPairsWithSameSum {
	public static void main(String[] args) {
		Integer[] lInputs = new Integer[] { 3, 4, 7, 1, 2, 9, 8 };
		Map<Integer, List<Integer>> lHashMapValue = processInput(lInputs);
		System.out.println(lHashMapValue);
		processHashMap(lHashMapValue);
		System.out.println(lHashMapValue);
	}

	private static void processHashMap(Map<Integer, List<Integer>> lHashMapValue) {
		// TODO Auto-generated method stub
		for(Integer key:lHashMapValue.keySet())
		{
			List<Integer> lList=lHashMapValue.get(key);
			if(lList.size()>2)
			{
				int size=lList.size();
				List<List<Integer>> listInteger=new ArrayList<List<Integer>>();
				for(int i=0;i<lList.size();i++)
				{
					List<Integer> lCombo=new ArrayList<Integer>();
					lCombo.add(lList.get(i));
					lCombo.add(lList.get(++i));
					listInteger.add(lCombo);
				}
				permutate(listInteger);
			}
		}
	}

	private static void permutate(List<List<Integer>> listInteger) {
		// TODO Auto-generated method stub
		for(int i=0;i<listInteger.size();i++)
		{
			for(int j=i+1;j<listInteger.size();j++)
			{
				System.out.println(listInteger.get(i)+" = "+listInteger.get(j));
			}
		}
	}

	private static Map<Integer, List<Integer>> processInput(Integer[] lInputs) {
		// TODO Auto-generated method stub
		List<Integer> lProcessedInputList = new ArrayList<Integer>();
		Map<Integer, List<Integer>> lHashMap = new HashMap<Integer, List<Integer>>();
		for (int i = 0; i < lInputs.length; i++) {
			for (int j = i + 1; j < lInputs.length; j++) {
				addInMap(lHashMap, lInputs[i] + lInputs[j], lInputs[i]);
				addInMap(lHashMap, lInputs[i] + lInputs[j], lInputs[j]);
			}
		}
		return lHashMap;
	}

	private static void addInMap(Map<Integer, List<Integer>> edgeListMap, Integer lKey, Integer lValues) {
		if (edgeListMap.keySet().contains(lKey)) {
			edgeListMap.get(lKey).add(lValues);
		} else {
			List<Integer> edgeListList = new ArrayList<Integer>();
			edgeListList.add(lValues);
			edgeListMap.put(lKey, edgeListList);
		}
	}
}
