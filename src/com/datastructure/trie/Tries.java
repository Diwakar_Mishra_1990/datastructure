package com.datastructure.trie;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Tries {
private final TrieNode root;
 public Tries() {
	// TODO Auto-generated constructor stub
	 root=new TrieNode();
}
 public static void main(String[] args) {
	Tries lTries=new Tries();
	lTries.insert("abc");
	lTries.insert("abgl");
	lTries.insert("cdf");
	lTries.insert("abcd");
	lTries.insert("lmn");
	TrieNode node= lTries.root;
	//
	while(!node.children.isEmpty())
	{
		System.out.println(node.children);
	}
	System.out.println(lTries);
}
 public void delete(String Word)
 {
	 TrieNode current=root;
	 String []wordArray=processWord(Word);
	 TrieNode temp=root;
	 //searching the word
	 boolean isAvailable=true;
	 Stack<TrieNode> lStack=new Stack<TrieNode>();
	 lStack.push(current);
	 for(int i=0;i<wordArray.length;i++)
	 {
		 TrieNode node=current.children.get(wordArray[i]);
		 if(node==null)
		 {
			 isAvailable=false;
			 lStack.clear();
			 System.out.println("NOT POSSIBLE AS WORD NOT PRESENT");
		 }
		 lStack.push(node);
		 current=node;
	 }
	 boolean removeParent=true;
	 while(!lStack.isEmpty())
	 {
		 TrieNode leafNode=lStack.pop();
		 if(leafNode.children.keySet().size()==0)
		 {
			 leafNode=null;
		 }
		 else
		 {
			 leafNode.isEndOfWord=true;
			 lStack.clear();
			 break;
		 }
	 }
	 /*if(current.children.keySet().size()==0)
	 {
		 temp.children.remove(wordArray[wordArray.length-1]);
		 current=null;
	 }
	 else
	 {
		 current.isEndOfWord=false;
	 }
	 current.isEndOfWord=true;*/
 }
 
 public void deletePrefix(String Word)
 {
	 TrieNode current=root;
	 String []wordArray=processWord(Word);
	 TrieNode temp=root;
	 for(int i=0;i<wordArray.length;i++)
	 {
		 TrieNode node=current.children.get(wordArray[i]);
		 if(node==null)
		 {
			 System.out.println("NOT POSSIBLE AS WORD NOT PRESENT");
		 }
		 temp=current;
		 current=node;
	 }
	 current.children.clear();
	 current.isEndOfWord=true;
	 
 }
 public void insert(String Word)
 {
	 TrieNode current=root;
	 String []wordArray=processWord(Word);
	 for(int i=0;i<wordArray.length;i++)
	 {
		 TrieNode node=current.children.get(wordArray[i]);
		 if(node==null)
		 {
			 node=new TrieNode();
			 current.children.put(wordArray[i], node);
		 }
		 current=node;
	 }
	 current.isEndOfWord=true;
 }
 public void searchWord(String Word)
 {
	 TrieNode current=root;
	 String []wordArray=processWord(Word);
	 for(int i=0;i<wordArray.length;i++)
	 {
		 TrieNode node=current.children.get(wordArray[i]);
		 if(node==null)
		 {
			 System.out.println("Not Found");
			 break;
		 }
		 current=node;
	 }
	 if(current.isEndOfWord)
	 {
		 System.out.println("Found");
	 }
	 else
	 {
		 System.out.println("NOT FOUND");
	 }
 }
 public void searchPrefix(String Word)
 {
	 TrieNode current=root;
	 String []wordArray=processWord(Word);
	 for(int i=0;i<wordArray.length;i++)
	 {
		 TrieNode node=current.children.get(wordArray[i]);
		 if(node==null)
		 {
			 System.out.println("Not Found");
			 break;
		 }
		 current=node;
	 }
	 System.out.println("PREFIX FOUND");
 }
private String[] processWord(String word) {
	// TODO Auto-generated method stub
	String s[]=new String[word.length()];
	for(int i=0;i<word.length();i++)
	{
		s[i]=word.charAt(i)+"";
	}
	return s;
}
}

class TrieNode
{
	Map<String,TrieNode> children;
	public Map<String, TrieNode> getChildren() {
		return children;
	}
	public void setChildren(Map<String, TrieNode> children) {
		this.children = children;
	}
	public boolean isEndOfWord() {
		return isEndOfWord;
	}
	public void setEndOfWord(boolean isEndOfWord) {
		this.isEndOfWord = isEndOfWord;
	}
	boolean isEndOfWord;
	public TrieNode() {
		children=new HashMap<String,TrieNode>();
		isEndOfWord=false;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return children.toString()+isEndOfWord;
	}
}
