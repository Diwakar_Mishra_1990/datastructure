package com.datastructure.SkyLineProblem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResolveSkyLineProblem {

	public static void main(String[] args) {
		// i/p takes the form start x coordinate, end x coordinate ,height
		String input = "1,3,3\n2,4,4\n5,8,2\n6,7,4\n8,9,4";
		//String input = "0,1,2\n0,2,3\n3,5,3\n4,5,2\n6,7,2\n7,8,3";
		//in case of c need to implement heap . Just making use of JAVA ccollections
		List<Position> inputs = processInputs(input);
		//Here i will be using java collections for heap . In real world we need to mak use of heap
		List<Integer> lHeapList=new ArrayList<Integer>();
		List<String> lSkyLine=new ArrayList<String>();
		lHeapList.add(0);
		System.out.println("######################################################");
		for(Position p:inputs)
		{
			//System.out.println(lSkyLine);
			int initialPriority=lHeapList.get(lHeapList.size()-1);
			if(p.start)
			{
				lHeapList.add(p.getY());
				Collections.sort(lHeapList);
				if(lHeapList.get(lHeapList.size()-1)!=initialPriority)
				{
					lSkyLine.add(p.getX()+"&"+lHeapList.get(lHeapList.size()-1));
				}
			}
			else
			{
				lHeapList.remove(lHeapList.indexOf(p.getY()));
				Collections.sort(lHeapList);
				if(lHeapList.get(lHeapList.size()-1)!=initialPriority)
				{
					lSkyLine.add(p.getX()+"&"+lHeapList.get(lHeapList.size()-1));
				}
			}
		}
		System.out.println(lSkyLine);
	}

	private static List<Position> processInputs(String input) {
		List<Position> lPosition = new ArrayList<Position>();
		String[] inputsDiv = input.split("\n");
		for (int i = 0; i < inputsDiv.length; i++) {
			String positions[] = inputsDiv[i].split(",");
			Position p = new Position(Integer.parseInt(positions[0]), Integer.parseInt(positions[2]), true);
			lPosition.add(p);
			p = new Position(Integer.parseInt(positions[1]), Integer.parseInt(positions[2]), false);
			lPosition.add(p);
		}
		Collections.sort(lPosition);
		printList(lPosition);
		return lPosition;
	}
	
	

	private static void printList(List<Position> lPosition) {
		// TODO Auto-generated method stub
		for(Position p:lPosition)
		{
			System.out.println(p);
		}
	}
}

class Position implements Comparable<Position> {
	public Position(int x, int y, boolean start) {
		super();
		this.x = x;
		this.y = y;
		this.start = start;
	}

	int x;
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}

	int y;
	boolean start;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if (this.start) {
			return "X=" + this.x + "\t Y=" + this.y + "\t  , s";
		}
		return "X=" + this.x + "\t Y=" + this.y + "\t  , e";
	}

	@Override
	public int compareTo(Position o) {
		if (o.x > this.x) {
			return -1;
		} else if (o.x == this.x) {
			if (o.start && !this.start) {
				return 1;
			} else if (o.start && this.start) {
				if (o.y > this.y) {
					return 1;
				} else if (o.y < this.y) {
					return -1;
				} else {
					return 0;
				}
			} else if (!o.start && !this.start) {
				if (o.y < this.y) {
					return 1;
				} else if (o.y > this.y) {
					return -1;
				} else {
					return 0;
				}
			} else {
				return -1;
			}
		}
		return 1;
	}

}
