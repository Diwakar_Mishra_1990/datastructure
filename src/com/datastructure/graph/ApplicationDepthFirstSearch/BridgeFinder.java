package com.datastructure.graph.ApplicationDepthFirstSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastructure.graph.BreadthFirstSearch.BFSTraversal;
import com.datastructure.graph.DepthFirstSearch.DepthFirstTraversal;
import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;

public class BridgeFinder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String edgeList="a,b\na,c\nb,c\nc,d\nd,e\nd,f\nf,e\ne,g\ng,h\nh,i\ni,g";
		Map<String,List<String>> adjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edgeList);
		Map<String,Integer> arrivalMap=new HashMap<String,Integer>();
		Map<String,Integer> departureMap=new HashMap<String,Integer>();
		Map<String,Integer> deepestBackEdge=new HashMap<String,Integer>();
		List<String> bfsTraversal=DepthFirstTraversal.performDfsTraversal(adjacencyList,arrivalMap,departureMap,deepestBackEdge);
	}


	private static Map<java.lang.Integer, java.lang.String> arrivalMapreverseHashMap(
			Map<java.lang.String, java.lang.Integer> arrivalMap) {
		HashMap<Integer, String> reversedHashMap = new HashMap<Integer, String>();
		for (String key : arrivalMap.keySet()){
		    reversedHashMap.put(arrivalMap.get(key), key);
		}
		return reversedHashMap;
	}

	

}
