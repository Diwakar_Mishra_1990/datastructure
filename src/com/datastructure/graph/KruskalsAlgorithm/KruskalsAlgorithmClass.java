package com.datastructure.graph.KruskalsAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastructure.graph.CreateDisjointSet.CreateDisJointSetClass;
import com.datastructure.graph.PriorityEdgeMap.PriorityEdgeMapClass;
import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

public class KruskalsAlgorithmClass {

	public static void main(String[] args) {
		String edges = "a,b,1\na,e,4\ne,d,6\na,f,8\nf,c,2\na,d,7\nb,c,5\nb,f,3\nd,c,9";
		Map<String, List<String>> lEdgePriorityMap = PriorityEdgeMapClass.pepareEdgeMapPriority(edges);
		List<String> priorityList = new ArrayList<String>(lEdgePriorityMap.keySet());
		Collections.sort(priorityList);
		String minimumSpanningTree = "";
		CreateDisJointSetClass lDisJointSet=new CreateDisJointSetClass();
		Integer minimumSpanningTreeWeight = 0;
		
		//make set of disjoint set on individual vertices
		for (String s : priorityList) {
			List<String> lEdges = lEdgePriorityMap.get(s);
			for (String edge : lEdges) {
				String[] Vertices = edge.split(",");
				lDisJointSet.makeSet(Vertices);
			}
		}
		//make union of individual edges  .In CreateDisJointSet we have added function for Kruskals algorithm
		for (String s : priorityList) {
			List<String> lEdges = lEdgePriorityMap.get(s);
			for (String edge : lEdges) {
				String[] Vertices = edge.split(",");
				if(!(lDisJointSet.findSet(Vertices[0]) ==lDisJointSet.findSet(Vertices[1])))
				{
					if(lDisJointSet.union(Vertices[0], Vertices[1],true))
					{
						minimumSpanningTree = minimumSpanningTree + Vertices[0] + "," + Vertices[1] + "," + s + "\n";
						minimumSpanningTreeWeight = minimumSpanningTreeWeight + Integer.parseInt(s);
					}
				}
				else
				{
					System.out.println("Cycle in graph");
				}
			}
		}
		System.out.println(minimumSpanningTree + "=====" + minimumSpanningTreeWeight);
	}


}
/*if (usedVertices.contains(Vertices[0]) && usedVertices.contains(Vertices[1])) {
System.out.println("Cycle is formend");
continue;
}
minimumSpanningTree = minimumSpanningTree + Vertices[0] + "," + Vertices[1] + "," + s + "\n";
minimumSpanningTreeWeight = minimumSpanningTreeWeight + Integer.parseInt(s);
usedVertices.add(Vertices[0]);
usedVertices.add(Vertices[1]);*/