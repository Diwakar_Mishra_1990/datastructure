package com.datastructure.graph.PrimsAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastructure.graph.CreateHeap.CreatHeapClass;
import com.datastructure.graph.PriorityEdgeMap.PriorityEdgeMapClass;
import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;

public class PrimsAlgorithm {

	public static void main(String[] args) {
		String edges = "a,b,1\na,c,3\na,d,11\nb,c,2\nb,e,9\nb,f,8\nc,f,7\nc,g,5\nc,d,13\nd,h,10\ne,f,12\nf,g,6\ng,h,4";
		String minimumSpanningTree="";
		findPrimsMinimumSpanningTree(edges, minimumSpanningTree);
	}

	private static void findPrimsMinimumSpanningTree(String edges, String minimumSpanningTree) {
		Map<Integer, List<String>> lEdgePriorityMap = PriorityEdgeMapClass.pepareEdgeMapPriorityInteger(edges);
		Map<String ,List<String>> lEdgeAdjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edges);
		List<Integer> lPriorityListParent=new ArrayList<Integer>(lEdgePriorityMap.keySet());
		Collections.sort(lPriorityListParent);
		Map<String, Integer> lReverseEdgePriorityMap=reverseMap(lEdgePriorityMap);
		List<String> usedVertices=new ArrayList<String>();
		List<String> unUsedVertices=new ArrayList<String>(lEdgeAdjacencyList.keySet());
		List<Integer> heapList=new ArrayList<Integer>();
		heapList.add(lPriorityListParent.get(0));
		String minimumVertices=lEdgePriorityMap.get(lPriorityListParent.get(0)).get(0).split(",")[0];
		usedVertices.add(minimumVertices);
		unUsedVertices.remove(minimumVertices);
		while(unUsedVertices.size()!=0)
		{
			List<String>adjacentVertices=lEdgeAdjacencyList.get(minimumVertices);
			for(int i=0;i<adjacentVertices.size();i++)
			{
				String edge=minimumVertices+","+adjacentVertices.get(i);
				String reverseEdge=adjacentVertices.get(i)+","+minimumVertices;
				if( !usedVertices.contains(adjacentVertices.get(i)) && lReverseEdgePriorityMap.containsKey(edge))
				{
					if(!heapList.contains(lReverseEdgePriorityMap.get(edge)))
					{
						CreatHeapClass.insertList(heapList,lReverseEdgePriorityMap.get(edge));
					}
					
				}
				else if(!usedVertices.contains(adjacentVertices.get(i)) && lReverseEdgePriorityMap.containsKey(reverseEdge))
				{
					if(!heapList.contains(lReverseEdgePriorityMap.get(reverseEdge)))
					{
						CreatHeapClass.insertList(heapList,lReverseEdgePriorityMap.get(reverseEdge));
					}
				}
				
			}
			String minimumEdge=lEdgePriorityMap.get(heapList.get(0)).get(0);
			if(usedVertices.contains(minimumEdge.split(",")[0]) && !usedVertices.contains(minimumEdge.split(",")[1]))
			{
				minimumVertices=minimumEdge.split(",")[0];
			}
			else if(usedVertices.contains(minimumEdge.split(",")[1]) && !usedVertices.contains(minimumEdge.split(",")[0]))
			{
				minimumVertices=minimumEdge.split(",")[1];
			}
			if(!usedVertices.contains(minimumEdge.split(",")[0]))
			{
				minimumSpanningTree=minimumSpanningTree+minimumVertices+",";
				minimumVertices=lEdgePriorityMap.get(heapList.get(0)).get(0).split(",")[0];
				usedVertices.add(minimumVertices);
				unUsedVertices.remove(minimumVertices);
				minimumSpanningTree=minimumSpanningTree+minimumVertices+"\n";
			}
			else if(!usedVertices.contains(minimumEdge.split(",")[1])){
				minimumSpanningTree=minimumSpanningTree+minimumVertices+",";
				minimumVertices=lEdgePriorityMap.get(heapList.get(0)).get(0).split(",")[1];
				usedVertices.add(minimumVertices);
				unUsedVertices.remove(minimumVertices);
				minimumSpanningTree=minimumSpanningTree+minimumVertices+"\n";
			}
			CreatHeapClass.deleteList(heapList,heapList.get(0));
		}
		System.out.println("**********************************************");
		System.out.println(minimumSpanningTree);
		System.out.println("**********************************************");
	}

	private static Map<String, Integer> reverseMap(Map<Integer, List<String>> lEdgePriorityMap) {
		Map<String, Integer> reverseMap=new HashMap <String, Integer>();
		for(Integer i:lEdgePriorityMap.keySet())
		{
			reverseMap.put(lEdgePriorityMap.get(i).get(0), i);
		}
		return reverseMap;
	}
}
