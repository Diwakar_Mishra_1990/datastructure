package com.datastructure.graph.adjacencylist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateAdjacencyListClass {

	public static void main(String[] args) {
		String edgeList = "1,2\n2,3\n3,4\n4,1\n1,3" ;
		createAdjecencyListForUndirectedGraph(edgeList);
	}

	public static Map<String ,List<String>> createAdjecencyListForUndirectedGraph(String edgeList) {
		// TODO Auto-generated method stub
		Map <String ,List<String>> edgeListMap=new HashMap<String ,List<String>>();
		String []edges=edgeList.split("\n");
		for(int i=0;i<edges.length;i++)
		{
			String []edgeVertices=edges[i].split(",");
			addInMap(edgeListMap, edgeVertices[0],edgeVertices[1]);
			addInMap(edgeListMap, edgeVertices[1],edgeVertices[0]);
		}
		System.out.println(edgeListMap);
		return edgeListMap;
	}
	
	public static Map<String ,List<String>> createAdjecencyListForDirectedGraph(String edgeList) {
		// TODO Auto-generated method stub
		Map <String ,List<String>> edgeListMap=new HashMap<String ,List<String>>();
		String []edges=edgeList.split("\n");
		for(int i=0;i<edges.length;i++)
		{
			String []edgeVertices=edges[i].split(",");
			addInMap(edgeListMap, edgeVertices[0],edgeVertices[1]);
		}
		System.out.println(edgeListMap);
		return edgeListMap;
	}

	private static void addInMap(Map<String, List<String>> edgeListMap, String incidentedgeVertices, String targetEdgeVertices) {
		if(edgeListMap.keySet().contains(incidentedgeVertices))
		{
			edgeListMap.get(incidentedgeVertices).add(targetEdgeVertices);
		}
		else
		{
			List <String>edgeListList=new ArrayList<String>();
			edgeListList.add(targetEdgeVertices);
			edgeListMap.put(incidentedgeVertices,edgeListList);
		}
	}

	private static void printArray(String[] edges) {
		for(int i=0;i<edges.length;i++)
		{
			System.out.println(edges[i]);
		}
	}
	private static void printArray1(String[] edges) {
		for(int i=0;i<edges.length;i++)
		{
			System.out.println(edges[i]);
		}
	}
}
