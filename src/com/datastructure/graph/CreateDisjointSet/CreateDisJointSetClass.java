package com.datastructure.graph.CreateDisjointSet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CreateDisJointSetClass {

	private Map <String,Node> lNodesMap=new HashMap<String,Node>();
	
	class Node{
		String data;
		Node parent;
		int rank;
	}
	
	public void makeSet(String[]data)
	{
		for (int i=0;i< data.length ;i++)
		{
			Node newNode=new Node();
			newNode.data=data[i];
			newNode.parent=newNode;	
			newNode.rank=0;
			//At starting each verices are its own parent and are self represntative
			lNodesMap.put(data[i],newNode);
		}
	}
	
	public void makeSet(List <String> data)
	{
		for (int i=0;i< data.size() ;i++)
		{
			Node newNode=new Node();
			newNode.data=data.get(i);
			newNode.parent=newNode;	
			newNode.rank=0;
			//At starting each verices are its own parent and are self representative
			lNodesMap.put(data.get(i),newNode);
		}
	}
	public void makeSet(String data)
	{
			Node newNode=new Node();
			newNode.data=data;
			newNode.parent=newNode;	
			newNode.rank=0;
			///At starting each verices are its own parent and are self represntative
			lNodesMap.put(data,newNode);
	}

	/**
	 * Modification only done for the sake of kruskals algorithm
	 * otherwise the method would be public void union(String data1 , String data2)
	 * @param data1
	 * @param data2
	 * @param isKruskalsAlgorithm
	 * @return
	 */
	public boolean union(String data1 , String data2 , boolean isKruskalsAlgorithm)
	{
		Node lNode1= lNodesMap.get(data1);
		Node lNode2=lNodesMap.get(data2);
		Node lParent1= findSet(lNode1);
		Node lParent2= findSet(lNode2);
		//condition added only for kruskals algorithm
		if(isKruskalsAlgorithm )
		{
			if(!(lParent1==lParent2))
			{
				mergeParents(lParent1, lParent2);
				return true;
			}
			else
			{
				System.out.println("CYCLE DETECTED AT TIME OF UNION");
				return false;
			}
			
		}
		
			mergeParents(lParent1, lParent2);
			return true;
		
		
	}

	private void mergeParents(Node lParent1, Node lParent2) {
		if(lParent1.rank>lParent2.rank)
		{
			lParent2.parent=lParent1;
			lNodesMap.put(lParent2.data, lParent2.parent);
		}
		else if(lParent1.rank==lParent2.rank)
		{
				lParent1.rank++;
				lParent2.parent=lParent1;
				lNodesMap.put(lParent2.data, lParent2.parent);
		}
		else
		{
			lParent1.parent=lParent2;
			lNodesMap.put(lParent1.data, lParent1.parent);
		}
	} 	
	private Node findSet(Node node)
	{
		Node lParent= node.parent;
		if(lParent==node)
		{
			return lParent;
		}
		/**
		 * Node compression takes at the time of union. this
		 * is a private method used at the time of union
		 */
		node.parent=findSet(node.parent);
		lNodesMap.put(node.data, node.parent);
		return node.parent;
	}
	/**
	 * Finds representative of the data
	 * @param data
	 * @return
	 */
	public String findSet(String data)
	{
		return lNodesMap.get(data).data;
	}
}
