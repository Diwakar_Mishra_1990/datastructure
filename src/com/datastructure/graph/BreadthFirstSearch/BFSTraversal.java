package com.datastructure.graph.BreadthFirstSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;

//Refer BFS.png for graph
public class BFSTraversal {
public static void main(String[] args) {
	String edges="s,r\nr,v\ns,w\nw,t\nw,x\nt,x\nt,u\nu,y\nx,y";
	Map <String ,List<String>> adjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edges);
	List<String> bfsVertexList=new ArrayList<String>();
	Map<String,Integer> bfsLevelMap=new HashMap<String,Integer>();
	bfsTraversal(adjacencyList,bfsVertexList,bfsLevelMap);
}

public static List<String> bfsTraversal(Map<String, List<String>> adjacencyList,List<String> bfsVertexList, Map<String,Integer> bfsLevelMap) {
	// TODO Auto-generated method stub
	Queue<String> bfsQueue=new LinkedList<String>();
	Map<String,String> vertexColor=new HashMap<String,String>();
	List<String> vertexlist= new ArrayList<String>(adjacencyList.keySet());
	bfsQueue.add(vertexlist.get(0));
	bfsLevelMap.put(vertexlist.get(0), 0);
	vertexColor.put(vertexlist.get(0), "G");
		while(!bfsQueue.isEmpty())
		{
			String bfsQueueBlack=bfsQueue.poll();
			if(vertexColor.containsKey(bfsQueueBlack) && 
					vertexColor.get(bfsQueueBlack).equals("B"))
			{
				continue;
			}
			List<String> adjacentList=adjacencyList.get(bfsQueueBlack);
			for(String value : adjacentList)
			{
				if(!vertexColor.containsKey(value))
				{
					vertexColor.put(value, "G");
				}
				if(vertexColor.containsKey(value)&& 
						!vertexColor.get(value).equals("B") &&
						!bfsQueue.contains(value))
				{
					int level=bfsLevelMap.get(bfsQueueBlack);
					bfsLevelMap.put(value,++level);
					bfsQueue.add(value);
				}
				
			}
			bfsVertexList.add(bfsQueueBlack);
			vertexColor.put(bfsQueueBlack, "B");
		}
	for(String key : bfsVertexList)
	{
		System.out.println(key);
	}
	return bfsVertexList;
}
}
