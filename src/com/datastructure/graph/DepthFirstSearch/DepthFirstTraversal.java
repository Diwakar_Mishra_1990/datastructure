package com.datastructure.graph.DepthFirstSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;

import sun.net.NetworkServer;

public class DepthFirstTraversal {
 public static void main(String[] args) {
	String edgeList="a,b\na,c\na,d\nb,e\nb,f\nb,c\nd,f\ne,f";
	Map<String,List<String>> adjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edgeList);
	Map<String,Integer> arrivalMap=new HashMap<String,Integer>();
	Map<String,Integer> departureMap=new HashMap<String,Integer>();
	Map<String,Integer> deepestBackEdge=new HashMap<String,Integer>();
	performDfsTraversal(adjacencyList,arrivalMap,departureMap,deepestBackEdge);
}

public static List<String> performDfsTraversal(Map<String, List<String>> adjacencyList, Map<String, Integer> arrivalMap,
		Map<String, Integer> departureMap,Map<String,Integer> deepestBackEdge) {
	// TODO Auto-generated method stub
	Stack<String> verticesStack = new Stack<String>();
	List<String> vertexlist= new ArrayList<String>(adjacencyList.keySet());
	verticesStack.push(vertexlist.get(0));
	Map<String,String> vertexColour=new HashMap<String,String>();
	Map<String,String> parentVertices=new HashMap<String,String>();
	List<String> treeEdge=new ArrayList<String>();
	List<String>bfsTraversal=new ArrayList<String>();
	String vertices=verticesStack.get(0);
	int level=0;
	arrivalMap.put(vertexlist.get(0), level);
	vertexColour.put(vertexlist.get(0), "G");
	int tempDeepestBackEdge=0;
	int deepestBackEdgeForVertices=2*adjacencyList.keySet().size();
	while(!verticesStack.isEmpty())
	{
		System.out.println(verticesStack);
		if(!bfsTraversal.contains(vertices))
		{
			bfsTraversal.add(vertices);
		}
		List<String> lAdjacencyList=adjacencyList.get(vertices);
		int count=0;
		//int tempDeepestBackEdge=deepestBackEdgeForVertices;
		while(arrivalMap.get(lAdjacencyList.get(count))!=null)
		{
			deepestBackEdgeForVertices=Math.min(deepestBackEdgeForVertices, arrivalMap.get(lAdjacencyList.get(count)));
			count++;
			if(count==lAdjacencyList.size())
			{
				break;
			}
			
		}
		if(tempDeepestBackEdge>deepestBackEdgeForVertices)
		{
			System.out.println("########################################"+vertices);
		}
		deepestBackEdge.put(vertices, deepestBackEdgeForVertices);
		if(count==lAdjacencyList.size())
		{
			if(!departureMap.containsKey(vertices))
			{
				departureMap.put(vertices,++level );
			}
			vertexColour.put(vertices, "B");
			vertices=verticesStack.pop();
			int backedge=deepestBackEdge.get(vertices);
			deepestBackEdgeForVertices=Math.min(backedge, deepestBackEdgeForVertices);
			deepestBackEdge.put(vertices,deepestBackEdgeForVertices);
		}
		else
		{
			int entry=++level;
			arrivalMap.put(lAdjacencyList.get(count),entry);
			tempDeepestBackEdge=deepestBackEdgeForVertices;
			if(!parentVertices.containsKey(lAdjacencyList.get(count)))
			{
				parentVertices.put(lAdjacencyList.get(count), vertices);
			}
			
			treeEdge.add(lAdjacencyList.get(count)+","+vertices);
			treeEdge.add(vertices+","+lAdjacencyList.get(count));
			verticesStack.push(lAdjacencyList.get(count));
			vertexColour.put(lAdjacencyList.get(count), "G");
			vertices = lAdjacencyList.get(count);
			deepestBackEdgeForVertices=entry;
			deepestBackEdge.put(lAdjacencyList.get(count),entry);
		}
	}
	System.out.println("adjacencyList :"+adjacencyList);
	System.out.println("arrivalMap :"+arrivalMap);
	System.out.println("departureMap :"+departureMap);
	System.out.println("bfsTraversal :"+bfsTraversal);
	System.out.println("deepestBackEdge :"+deepestBackEdge);
	System.out.println(treeEdge);
	displayUnique(deepestBackEdge,parentVertices);
	return bfsTraversal;
}

private static void displayUnique(Map<String, Integer> deepestBackEdge, Map<String, String> parentVertices) {
	// TODO Auto-generated method stub
	Map<Integer,List<String>> revdepList=new HashMap<Integer,List<String>>();
	for(String e:deepestBackEdge.keySet())
	{
		if(!revdepList.containsKey(deepestBackEdge.get(e)))
		{
			revdepList.put(deepestBackEdge.get(e), new ArrayList<String>());
			revdepList.get(deepestBackEdge.get(e)).add(e);
		}
		else
		{
			revdepList.get(deepestBackEdge.get(e)).add(e);
		}
		
	}
	
	System.out.println(revdepList);
	List<String> bridgelist=new ArrayList<String>();
	for(Integer e:revdepList.keySet())
	{
		if (revdepList.get(e).size()==1)
		{
			bridgelist.add(revdepList.get(e).get(0));
		}
	}
	for(String e : bridgelist)
	{
		System.out.println(parentVertices.get(e)+","+e);
	}
	System.out.println(parentVertices);
}
 

}
