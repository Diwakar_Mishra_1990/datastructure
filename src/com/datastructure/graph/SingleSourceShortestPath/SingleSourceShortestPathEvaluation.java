package com.datastructure.graph.SingleSourceShortestPath;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;
import com.sun.xml.internal.ws.util.StringUtils;

/**
 * @author Diwakar Mishra
 *
 */
public class SingleSourceShortestPathEvaluation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String edges = "a,b,5\nb,c,2\na,e,2\na,d,9\nc,d,3\nd,f,2\nf,e,3";
		String source = "a";
		Map<String, List<String>>  lAdjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edges);
		List<Vertices> lHeapList = new ArrayList<Vertices>();
		List<Edge> lEdgeDistanceMap = createEdgeDistanceMap(edges);
		List<Vertices> lVertexList = instantiateHeapMap(edges);
		List<Vertices> lDistanceMap = new ArrayList<Vertices>();
		List<Vertices> lPathMap = new ArrayList<Vertices>();
		Vertices lSource = new Vertices();
		lSource.setVerticesNames(source);
		Vertices lVertex = lVertexList.get(lVertexList.indexOf(lSource));
		lVertex.setDistance(0);
		lVertex.setParentEdge(null);
		insertList(lHeapList, lVertex);
		int distance=0;
		while(lVertexList.size()!=0)
		{
			List<Vertices> lAssociatedVertices=findAssociatedVerticesOfVertex(lVertex,lAdjacencyList);
			for(Vertices assocVertiex:lAssociatedVertices)
			{
				if(lVertexList.indexOf(assocVertiex)>=0)
				{
					Vertices v=lVertexList.get(lVertexList.indexOf(assocVertiex));
					Edge e=new Edge();
					e.setEdges(lVertex.getVerticesNames()+","+v.getVerticesNames());
					int assocVerticesDistancedistance=distance+lEdgeDistanceMap.get(lEdgeDistanceMap.indexOf(e)).getDistance();
					if(v.getDistance()>assocVerticesDistancedistance)
					{
						
						v.setDistance(assocVerticesDistancedistance);
						v.setParentEdge(lVertex.getVerticesNames());
					}
					if(!lHeapList.contains(v))
					{
						insertList(lHeapList, v);
					}
				}
			}
			deleteList(lHeapList, lVertex);
			lPathMap.add(lVertex);
			lDistanceMap.add(lVertex);
			lVertexList.remove(lVertex);
			if(lHeapList.size()>0)
			{
				lVertex=lHeapList.get(0);
			}
			distance=lVertex.getDistance();
		}
System.out.println(lPathMap);
	}

	public static List<Vertices> deleteList(List<Vertices> heaplist, Vertices data) {

		int pos = findDataPositioninHeapList(heaplist, data);
		if (pos >= 0) {
			heaplist.set(pos, heaplist.get(heaplist.size() - 1));
			heaplist.remove(heaplist.size() - 1);
			shallowHeapifyList(heaplist, pos);
		}
		return heaplist;
	}
	
	private static int findDataPositioninHeapList(List<Vertices> vertices, Vertices data) {
		int index = -1;
		for (int i = 0; i < vertices.size(); i++) {
			if (vertices.get(i).equals(data)) {
				index = i;
			}
		}
		return index;
	}

	private static List<Vertices> findAssociatedVerticesOfVertex(Vertices lVertex, Map<String, List<String>> lAdjacencyList) {
		List<Vertices> lAssociatedVertices=new ArrayList<Vertices>();
		List<String> lAssociaedVerticesList=lAdjacencyList.get(lVertex.getVerticesNames());
		for(String s:lAssociaedVerticesList)
		{
			Vertices v=new Vertices();
			v.setVerticesNames(s);
			lAssociatedVertices.add(v);
		}
		return lAssociatedVertices;
	}

	/**
	 * @param medges
	 */
	private static List<Edge> createEdgeDistanceMap(String medges) {
		List<Edge> lEdgeList = new ArrayList<Edge>();
		String[] edgepart = medges.split("\n");
		for (int i = 0; i < edgepart.length; i++) {
			String[] edgeConfig = edgepart[i].split(",");
			Edge newEdge = new Edge();
			newEdge.setDistance(Integer.parseInt(edgeConfig[2]));
			newEdge.setEdges(edgeConfig[0] + "," + edgeConfig[1]);
			lEdgeList.add(newEdge);
		}
		System.out.println(lEdgeList);
		return lEdgeList;
	}

	/**
	 * @param medges
	 */
	private static List<Vertices> instantiateHeapMap(String medges) {
		List<Vertices> lVertices = new ArrayList<Vertices>();
		String[] edgepart = medges.split("\n");
		for (int i = 0; i < edgepart.length; i++) {
			String[] edgeConfig = edgepart[i].split(",");
			Vertices newVertices = new Vertices();
			newVertices.setVerticesNames(edgeConfig[0]);
			newVertices.setDistance(Integer.MAX_VALUE);
			if (!lVertices.contains(newVertices)) {
				lVertices.add(newVertices);
			}
			newVertices = new Vertices();
			newVertices.setVerticesNames(edgeConfig[1]);
			newVertices.setDistance(Integer.MAX_VALUE);
			if (!lVertices.contains(newVertices)) {
				lVertices.add(newVertices);
			}
		}
		System.out.println(lVertices);
		return lVertices;
	}

	/**
	 * @param mVertices
	 * @param data
	 * @return
	 */
	public static List<Vertices> insertList(List<Vertices> mVertices, Vertices data) {

		mVertices.add(data);
		for (int i = mVertices.size() - 1; i >= 0; i--) {
			// the last element is obviously a child of the tree
			Integer childPosition = i;
			Integer parentPosition = 2 * i;
			while (parentPosition != 0) {
				parentPosition = (childPosition - 1) / 2;
				if (mVertices.get(parentPosition).getDistance() > mVertices.get(childPosition).getDistance()) {
					swapList(mVertices, childPosition, parentPosition);
					shallowHeapifyList(mVertices, parentPosition);
				}
				childPosition = parentPosition;
			}
		}
		return mVertices;
	}

	/**
	 * @param mVertices
	 * @param childPosition
	 * @param parentPosition
	 */
	private static void swapList(List<Vertices> mVertices, Integer childPosition, Integer parentPosition) {
		Vertices temp = mVertices.get(parentPosition);
		mVertices.set(parentPosition, mVertices.get(childPosition));
		mVertices.set(childPosition, temp);
	}

	/**
	 * @param mVertices
	 * @param position
	 */
	private static void shallowHeapifyList(List<Vertices> mVertices, int position) {
		for (int i = position; i < mVertices.size(); i++) {
			if ((mVertices.size() - 1 >= 2 * i + 1)
					&& (mVertices.get(i).getDistance() > mVertices.get(2 * i + 1).getDistance())) {
				swapList(mVertices, i, 2 * i + 1);
			}
			if ((mVertices.size() - 1 >= 2 * i + 2)
					&& (mVertices.get(i).getDistance() > mVertices.get(2 * i + 2).getDistance())) {
				swapList(mVertices, i, 2 * i + 2);
			}
		}
	}
}

/**
 * @author Diwakar Mishra
 *
 */
class Edge {
	int distance;

	/**
	 * @return
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * @param i
	 */
	public void setDistance(int i) {
		this.distance = i;
	}

	/**
	 * @return
	 */
	public String getEdges() {
		return edges;
	}

	/**
	 * @param string
	 */
	public void setEdges(String string) {
		this.edges = string;
	}

	String edges;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return edges + distance;
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		String s=new StringBuffer(((Edge) obj).getEdges()).reverse().toString();
		if (obj instanceof Edge && (((Edge) obj).getEdges().equals(this.getEdges())||
				s.equals(this.getEdges()))) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 1;
	}
}

/**
 * 
 * @author Diwakar Mishra
 *
 */

class Vertices {
	String parentEdge;

	/**
	 * @return
	 */
	public String getParentEdge() {
		return parentEdge;
	}

	/**
	 * @param parentEdge
	 */
	public void setParentEdge(String parentEdge) {
		this.parentEdge = parentEdge;
	}

	/**
	 * @return
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * @param distance
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * @return
	 */
	public String getVerticesNames() {
		return verticesNames;
	}

	/**
	 * @param verticesNames
	 */
	public void setVerticesNames(String verticesNames) {
		this.verticesNames = verticesNames;
	}

	int distance;
	String verticesNames;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (obj instanceof Vertices && ((Vertices) obj).getVerticesNames().equals(this.getVerticesNames())) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.getParentEdge().hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "["+this.getVerticesNames() + "," + this.getDistance()+","+this.getParentEdge()+"]";
	}
}
