package com.datastructure.graph.CreateHeap;

import java.util.ArrayList;
import java.util.List;

public class CreatHeapClass {

	
	public static void main(String[] args) {
		Integer[] heaptree = heapifyExistingArray(
				new Integer[] { 31, 23, 22, 67, 11, 111, 1111, 11111, 22222, 3333, 5, 12, 89, 70, 1 }, true);
		printArray(heaptree);
	}
	/**
	 * 
	 * =========================================================================
	 * =============================== Following are sets of util methods for
	 * converting an array into a heap tree
	 * =========================================================================
	 * ===============================
	 */

	/**
	 * ====================================THESE SET OF FUNCTION ARE FOR
	 * ARRAYS===================================
	 */
	private static Integer[] heapifyExistingArray(Integer[] integers, boolean isMinHeap) {

		shallowHeapifyArray(integers, 0);
		// Start from the last element
		for (int i = integers.length - 1; i >= 0; i--) {
			// the last element is obviously a child of the tree
			Integer childPosition = i;
			Integer parentPosition = 2 * i;
			while (parentPosition != 0) {
				parentPosition = (childPosition - 1) / 2;
				if (integers[parentPosition] > integers[childPosition]) {
					swapArray(integers, childPosition, parentPosition);
					shallowHeapifyArray(integers, parentPosition);
				}
				childPosition = parentPosition;
			}
		}
		return integers;
	}

	private static Integer[] insertArray(Integer[] integers, int data) {

		Integer[] newIntegerArray = new Integer[integers.length + 1];
		// copy all the elements of exising integer to the new integer of one
		// size bigger
		copyArrayForInsert(integers, newIntegerArray, 0, integers.length - 1);
		// Start from the last element
		newIntegerArray[newIntegerArray.length - 1] = data;
		for (int i = integers.length - 1; i >= 0; i--) {
			// the last element is obviously a child of the tree
			Integer childPosition = i;
			Integer parentPosition = 2 * i;
			while (parentPosition != 0) {
				parentPosition = (childPosition - 1) / 2;
				if (integers[parentPosition] > integers[childPosition]) {
					swapArray(integers, childPosition, parentPosition);
					shallowHeapifyArray(integers, parentPosition);
				}
				childPosition = parentPosition;
			}
		}
		return integers;
	}

	/**
	 * The array needs to be heap sorted
	 * 
	 * @param integers
	 * @param data
	 * @return
	 */
	private static Integer[] deleteList(Integer[] integers, int data) {

		int pos = findDataPositioninHeapArray(integers, data);
		if (pos >= 0) {
			integers[pos] = integers[integers.length - 1];
			Integer[] newIntegerArray = new Integer[integers.length - 2];
			copyArrayForInsert(integers, newIntegerArray, 0, newIntegerArray.length - 1);
			shallowHeapifyArray(integers, pos);
		}
		return integers;
	}

	private static int findDataPositioninHeapArray(Integer[] integers, int data) {
		int index = -1;
		for (int i = 0; i < integers.length; i++) {
			if (integers[i] == data) {
				index = i;
			}
		}
		return index;

	}

	private static void copyArrayForInsert(Integer[] sourceIntegersArray, Integer[] targetIntegersArray, int startIndex,
			int endIndex) {
		// TODO Auto-generated method stub
		for (int i = startIndex; i <= endIndex; i++) {
			targetIntegersArray[i] = sourceIntegersArray[i];
		}
	}

	private static void printArray(Integer[] heaptree) {
		for (int i = 0; i < heaptree.length; i++) {
			System.out.print(heaptree[i] + ",");
		}
		System.out.println("");
	}

	private static void shallowHeapifyArray(Integer[] integers, int position) {
		for (int i = position; i < integers.length; i++) {
			if ((integers.length - 1 > 2 * i + 1) && (integers[i] > integers[2 * i + 1])) {
				swapArray(integers, i, 2 * i + 1);
			}
			if ((integers.length - 1 > 2 * i + 2) && (integers[i] > integers[2 * i + 2])) {
				swapArray(integers, i, 2 * i + 2);
			}
		}
		printArray(integers);
	}

	private static void swapArray(Integer[] integers, Integer childPosition, Integer parentPosition) {
		int temp = integers[parentPosition];
		integers[parentPosition] = integers[childPosition];
		integers[childPosition] = temp;
	}

	/**
	 * =======================================The NExt set of functions are all
	 * for list====================
	 */

	private static List<Integer> heapifyExistingList(List<Integer> integers, boolean isMinHeap) {

		shallowHeapifyList(integers, 0);
		// Start from the last element
		for (int i = integers.size() - 1; i >= 0; i--) {
			// the last element is obviously a child of the tree
			Integer childPosition = i;
			Integer parentPosition = 2 * i;
			while (parentPosition != 0) {
				parentPosition = (childPosition - 1) / 2;
				if (integers.get(parentPosition) > integers.get(childPosition)) {
					swapList(integers, childPosition, parentPosition);
					shallowHeapifyList(integers, parentPosition);
				}
				childPosition = parentPosition;
			}
		}
		return integers;
	}

	/**
	 * The input list needs to be heap sorted
	 * 
	 */
	public static List<Integer> deleteList(List<Integer> integers, int data) {

		int pos = findDataPositioninHeapList(integers, data);
		if (pos >= 0) {
			integers.set(pos, integers.get(integers.size() - 1));
			integers.remove(integers.size() - 1);
			shallowHeapifyList(integers, pos);
		}
		return integers;
	}

	private static int findDataPositioninHeapList(List<Integer> integers, int data) {

		int index = -1;
		for (int i = 0; i < integers.size(); i++) {
			if (integers.get(i) == data) {
				index = i;
			}
		}
		return index;
	}

	public static List<Integer> insertList(List<Integer> integers, int data) {

		integers.add(data);
		for (int i = integers.size() - 1; i >= 0; i--) {
			// the last element is obviously a child of the tree
			Integer childPosition = i;
			Integer parentPosition = 2 * i;
			while (parentPosition != 0) {
				parentPosition = (childPosition - 1) / 2;
				if (integers.get(parentPosition) > integers.get(childPosition)) {
					swapList(integers, childPosition, parentPosition);
					shallowHeapifyList(integers, parentPosition);
				}
				childPosition = parentPosition;
			}
		}
		return integers;
	}

	private static void shallowHeapifyList(List<Integer> integers, int position) {
		for (int i = position; i < integers.size(); i++) {
			if ((integers.size() - 1 >= 2 * i + 1) && (integers.get(i) > integers.get(2 * i + 1))) {
				swapList(integers, i, 2 * i + 1);
			}
			if ((integers.size() - 1 >= 2 * i + 2) && (integers.get(i) > integers.get(2 * i + 2))) {
				swapList(integers, i, 2 * i + 2);
			}
		}
		System.out.println(integers);
	}

	private static void swapList(List<Integer> integers, Integer childPosition, Integer parentPosition) {
		int temp = integers.get(parentPosition);
		integers.set(parentPosition, integers.get(childPosition));
		integers.set(childPosition, temp);
	}

}
