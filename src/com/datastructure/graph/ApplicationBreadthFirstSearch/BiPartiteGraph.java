package com.datastructure.graph.ApplicationBreadthFirstSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastructure.graph.BreadthFirstSearch.BFSTraversal;
import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;

public class BiPartiteGraph {
public static void main(String[] args) {
	String edges="s,r\nr,v\ns,w\nw,t\nw,x\nt,x\nt,u\nu,y\nx,y";
	Map <String ,List<String>> adjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edges);
	List<String> bfsVertexList=new ArrayList<String>();
	Map<String,Integer> bfsLevelMap=new HashMap<String,Integer>();
	BFSTraversal.bfsTraversal(adjacencyList,bfsVertexList,bfsLevelMap);
	System.out.println(bfsLevelMap);
	String []edgesList=edges.split("\n");
	boolean isBipartite=true;
	for(int i=0;i<edgesList.length;i++)
	{
		String []edgeVertices=edgesList[i].split(",");
		if(bfsLevelMap.get(edgeVertices[0])==(bfsLevelMap.get(edgeVertices[1])))
				{
			isBipartite=false;
			break;
				}
	}
	if(isBipartite)
	{
		System.out.println("GAPH IS BIPARTITE");
	}
	else
	{
		System.out.println("GAPH IS NOT BIPARTITE");
	}
}
}
