package com.datastructure.graph.ApplicationBreadthFirstSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastructure.graph.BreadthFirstSearch.BFSTraversal;
import com.datastructure.graph.adjacencylist.CreateAdjacencyListClass;

public class ConnectedComponents {

	public static void main(String[] args) {
		String edgeList="a,b\nb,c\nc,d\nd,e\ne,f\nf,a\na,e\nb,e\ng,h\nh,i\ni,g\nj,k\nk,l\nl,m\nm,j\nm,k";
		Map <String ,List<String>> adjacencyList=CreateAdjacencyListClass.createAdjecencyListForUndirectedGraph(edgeList);
		List<String> BFSTraversalList=new ArrayList<String>();
		Map <String ,Integer> bfsLevelMap=new HashMap<String,Integer>();
		Map <String ,List<String>> tempAdjacencyList=adjacencyList;
		int count=0;
		while (!tempAdjacencyList.isEmpty())
		{
			if(count>0)
			{
				System.out.println("GRAPH IS DISCONNECTED");
			}
			BFSTraversalList=BFSTraversal.bfsTraversal(tempAdjacencyList,BFSTraversalList,bfsLevelMap);
			for(String key:BFSTraversalList)
			{
				tempAdjacencyList.remove(key);
			}
			count++;
		}
		System.out.println("There is "+count+" connected components");	
	}
}
