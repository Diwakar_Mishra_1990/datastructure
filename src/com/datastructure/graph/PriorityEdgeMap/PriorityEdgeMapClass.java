package com.datastructure.graph.PriorityEdgeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PriorityEdgeMapClass {
	public static Map<String, List<String>> pepareEdgeMapPriority(String edges) {
		String edgesWeight[] = edges.split("\n");
		Map<String, List<String>> lEdgePriorityMap = new HashMap<String, List<String>>();
		for (int i = 0; i < edgesWeight.length; i++) {
			String individualEdgeWeight[] = edgesWeight[i].split(",");

			if (lEdgePriorityMap.containsKey(individualEdgeWeight[2])) {
				List<String> lEdgeListIng = lEdgePriorityMap.get(individualEdgeWeight[2]);
				if (!lEdgeListIng.contains(individualEdgeWeight[0] + "," + individualEdgeWeight[1])
						&& !lEdgeListIng.contains(individualEdgeWeight[1] + "," + individualEdgeWeight[0])) {
					lEdgeListIng.add(individualEdgeWeight[0] + "," + individualEdgeWeight[1]);
					lEdgePriorityMap.put(individualEdgeWeight[2], lEdgeListIng);
				}

			} else {
				List<String> lEdgeList = new ArrayList<String>();
				lEdgeList.add(individualEdgeWeight[0] + "," + individualEdgeWeight[1]);
				lEdgePriorityMap.put(individualEdgeWeight[2], lEdgeList);
			}

		}
		System.out.println(lEdgePriorityMap);
		return lEdgePriorityMap;
	}
	public static Map<Integer, List<String>> pepareEdgeMapPriorityInteger(String edges) {
		String edgesWeight[] = edges.split("\n");
		Map<Integer, List<String>> lEdgePriorityMap = new HashMap<Integer, List<String>>();
		for (int i = 0; i < edgesWeight.length; i++) {
			String individualEdgeWeight[] = edgesWeight[i].split(",");

			if (lEdgePriorityMap.containsKey(individualEdgeWeight[2])) {
				List<String> lEdgeListIng = lEdgePriorityMap.get(individualEdgeWeight[2]);
				if (!lEdgeListIng.contains(individualEdgeWeight[0] + "," + individualEdgeWeight[1])
						&& !lEdgeListIng.contains(individualEdgeWeight[1] + "," + individualEdgeWeight[0])) {
					lEdgeListIng.add(individualEdgeWeight[0] + "," + individualEdgeWeight[1]);
					lEdgePriorityMap.put(Integer.parseInt(individualEdgeWeight[2]), lEdgeListIng);
				}

			} else {
				List<String> lEdgeList = new ArrayList<String>();
				lEdgeList.add(individualEdgeWeight[0] + "," + individualEdgeWeight[1]);
				lEdgePriorityMap.put(Integer.parseInt(individualEdgeWeight[2]), lEdgeList);
			}

		}
		System.out.println(lEdgePriorityMap);
		return lEdgePriorityMap;
	}
}
