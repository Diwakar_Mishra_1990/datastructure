package com.datastructure.DynamicProgramming.Fibonaci;

public class NumberWithoutConsecutive1inBinaryRepresentation {
public static void main(String[] args) {
	int inputs=10;
	getNoOfDigitsWithoutConsecutive1s(inputs);
}

private static void getNoOfDigitsWithoutConsecutive1s(int inputs) {
	// TODO Auto-generated method stub
	System.out.println("No Of Bits. -"+inputs);
	System.out.println("Range Sarts from 0 to "+Math.pow(2, inputs));
	int result=0;
	int n0=2;//When n=0 ie inputs=0 1 digit is there and surely we will have no consecutive 1's 
	//so 2 inputs 0 and 1 does not have consecutive 1's
	int n1=3;////When n=1 ie inputs=1 2 digit is there and surely we will have no consecutive 1's other than 11
	for(int i=2;i<=10;i++)
	{
		result=n0+n1;
		n0=n1;
		n1=result;
	}
	System.out.println("No of bits without consecutive 1's ="+result);
}
}
