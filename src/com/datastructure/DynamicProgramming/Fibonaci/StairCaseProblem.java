package com.datastructure.DynamicProgramming.Fibonaci;

/**
 * @author Diwakar Mishra
 *Consider that at a time you can only jump one or two step at a time
 */
public class StairCaseProblem {
	public static void main(String[] args) {
		int inputs=10;
		getNoOfWaysToReachTop(inputs);
	}

	private static void getNoOfWaysToReachTop(int inputs) {
		// TODO Auto-generated method stub
		System.out.println("No Of Steps. -"+inputs);
		int result=0;
		int n1=1;//When n=1 ie inputs=1 in 1 attempt you can reach top of one step  
		//so 2 inputs 0 and 1 does not have consecutive 1's
		int n2=2;////When n=2 ie inputs=2 there are two ways you can reach at top of the step either by hopping
		// 1,1 or directly 2
		for(int i=2;i<=inputs;i++)
		{
			result=n1+n2;
			n1=n2;
			n2=result;
		}
		System.out.println("No of ways to jump at top of "+inputs+" step = "+result);
	}
}
