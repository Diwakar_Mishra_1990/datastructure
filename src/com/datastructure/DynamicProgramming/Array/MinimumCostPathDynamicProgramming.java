package com.datastructure.DynamicProgramming.Array;

public class MinimumCostPathDynamicProgramming {
public static void main(String[] args) {
	Integer[][] lInputs=new Integer[][]{{0,0,0,0,0},{0,1,3,5,8},{0,4,2,1,7},{0,4,3,2,3}};
	printArray(lInputs);
	Integer[][] lDynamicMap=createDynamicArrayMap(lInputs);
	
	
}
private static Integer[][] createDynamicArrayMap(Integer[][] lInputs) {
	Integer[][] lDynamicMap=lInputs;
	for(int i=1;i<lInputs.length;i++)
	{
		for(int j=1;j<lInputs[0].length;j++)
		{
			if(i-1==0 )
			{
				lDynamicMap[i][j]=lDynamicMap[i][j-1]+lDynamicMap[i][j];
			}
			if(j-1==0)
			{
				lDynamicMap[i][j]=lDynamicMap[i-1][j]+lDynamicMap[i][j];
			}
			else {
				int minimum=Math.min(lInputs[i-1][j], lInputs[i][j-1]);
				if(minimum==lInputs[i-1][j])
				{
					lDynamicMap[i][j]=lDynamicMap[i-1][j]+lDynamicMap[i][j];
				}
				else if(minimum==lInputs[i][j-1])
				{
					lDynamicMap[i][j]=lDynamicMap[i][j-1]+lDynamicMap[i][j];
				}
			}
		}
	}
	
	
	System.out.println("************************");
	printArray(lDynamicMap);
	return lDynamicMap;
}
private static void printArray(Integer[][] lDynamicArrayMap) {
	// TODO Auto-generated method stub
	for (int i = 0; i < lDynamicArrayMap.length; i++) {
		for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
			System.out.print(lDynamicArrayMap[i][j] + "\t");
		}
		System.out.println("");
	}
}
}
