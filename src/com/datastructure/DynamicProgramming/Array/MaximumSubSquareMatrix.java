package com.datastructure.DynamicProgramming.Array;

public class MaximumSubSquareMatrix {

	public static void main(String[] args) {
		Integer[][] lInputs = new Integer[][] { { 0, 0, 1, 1, 1 },
				{ 1, 0, 1, 1, 1 }, { 0, 1, 1, 1, 1 }, { 1, 0, 1, 1, 1 } };
		printArray(lInputs);
		Integer[][] lDynamicMap = createDynamicArrayMap(lInputs);
		processDynamicArray(lDynamicMap);
	}

	private static void processDynamicArray(Integer[][] lDynamicMap) {
		// TODO Auto-generated method stub
		System.out.println("MAximum subsquare");
		System.out.println(lDynamicMap[lDynamicMap.length-1][lDynamicMap[lDynamicMap.length-1].length-1]);
	}

	private static Integer[][] createDynamicArrayMap(Integer[][] lInputs) {
		Integer[][] lDynamicMap = new Integer[lInputs.length + 1][lInputs[0].length + 1];
		lDynamicMap[0][0] = 0;
		for (int i = 1; i < lDynamicMap.length; i++) {
			lDynamicMap[i][0] = 0;
		}
		for (int i = 1; i < lDynamicMap[0].length; i++) {
			lDynamicMap[0][i] = 0;
		}
		for (int i = 1; i < lDynamicMap.length; i++) {
			for (int j = 1; j < lDynamicMap[i].length; j++) {
				if (lInputs[i - 1][j - 1] == 0) {
					lDynamicMap[i][j] = 0;
				} else {
					lDynamicMap[i][j] = 1 + Math.min(Math.min(
							lDynamicMap[i - 1][j], lDynamicMap[i][j - 1]),
							lDynamicMap[i - 1][j - 1]);
				}
				System.out.println("*******************");
				printArray(lDynamicMap);
				/*
				 * if(i-1==0 ) {
				 * lDynamicMap[i][j]=lDynamicMap[i][j-1]+lDynamicMap[i][j]; }
				 * if(j-1==0) {
				 * lDynamicMap[i][j]=lDynamicMap[i-1][j]+lDynamicMap[i][j]; }
				 * else { int minimum=Math.min(lInputs[i-1][j],
				 * lInputs[i][j-1]); if(minimum==lInputs[i-1][j]) {
				 * lDynamicMap[i][j]=lDynamicMap[i-1][j]+lDynamicMap[i][j]; }
				 * else if(minimum==lInputs[i][j-1]) {
				 * lDynamicMap[i][j]=lDynamicMap[i][j-1]+lDynamicMap[i][j]; } }
				 */}
		}

		System.out.println("************************");
		printArray(lDynamicMap);
		return lDynamicMap;
	}

	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
