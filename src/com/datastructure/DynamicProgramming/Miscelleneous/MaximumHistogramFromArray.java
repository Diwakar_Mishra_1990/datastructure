package com.datastructure.DynamicProgramming.Miscelleneous;

public class MaximumHistogramFromArray {

	public static void main(String[] args) {
		Integer [][]inputs=new Integer[][]{{1,0,0,1,1,1},{1,0,1,1,1,1},{0,1,1,1,1,1},{0,0,1,1,1,1}};
		System.out.println(createAndProcessDynamicArray(inputs));
	}
	private static int createAndProcessDynamicArray(Integer[][] inputs) {
		Integer[]  modifiedArrayForRows=new Integer[inputs[0].length];
		int maxArea=0;
		for(int j=0;j<inputs[0].length;j++)
		{
			modifiedArrayForRows[j]=0;
		}
		for(int i=0;i<inputs.length;i++)
		{
			for(int j=0;j<inputs[0].length;j++)
			{
				if(inputs[i][j]==0)
				{
					modifiedArrayForRows[j]=0;
				}
				else
				{
					modifiedArrayForRows[j]=modifiedArrayForRows[j]+inputs[i][j];
				}
			}
			int temp=calculateMaxAreaOfHistogram(modifiedArrayForRows);
			if(temp>maxArea);
			{
				maxArea=temp;
			}
		}
		return maxArea;
	}
	private static int calculateMaxAreaOfHistogram(
			Integer[] modifiedArrayForRows) {
		int area=0;
		for(int i=0;i<modifiedArrayForRows.length;i++)
		{
			if(modifiedArrayForRows[i]==0)
			{
				continue;
			}
			else
			{
				int k=i-1;
				int j=i+1;
				int count=1;
				while(k>=0 && modifiedArrayForRows[k]>=modifiedArrayForRows[i])	
				{
					count++;
					k--;
				}
				while(j<modifiedArrayForRows.length && modifiedArrayForRows[j]>=modifiedArrayForRows[i])	
				{
					count++;
					j++;
				}
				if(count*modifiedArrayForRows[i]>area)
				{
					area=count*modifiedArrayForRows[i];
				}
			}
		}
		return area;
	}
	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		System.out.println("***************************************");
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");

		}
		System.out.println("");
	}
	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		System.out.println("***************************************");
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
