package com.datastructure.DynamicProgramming.Miscelleneous;

import java.util.HashMap;
import java.util.Map;

import javax.management.openmbean.KeyAlreadyExistsException;

public class MaximumSumAlgorithmRectangularSubMAtrix {
	public static void main(String[] args) {
		Integer[][] inputs = new Integer[][] { { 2, 1, -3, -4, 5 }, { 0, 6, 3, 4, 1 }, { 2, -2, -1, 4, -5 },
				{ -3, 3, 1, 0, 3 } };
		Map<String, Integer> lPositionMap = new HashMap<String, Integer>();
		lPositionMap.put("LEFT", 0);
		lPositionMap.put("RIGHT", 0);
		lPositionMap.put("TOP", 0);
		lPositionMap.put("DOWN", 0);
		lPositionMap.put("CURR_SUM", 0);
		lPositionMap.put("MAX_SUM", 0);
		lPositionMap.put("MAX_LEFT", 0);
		lPositionMap.put("MAX_RIGHT", 0);
		lPositionMap.put("MAX_TOP", 0);
		lPositionMap.put("MAX_DOWN", 0);
		printArray(inputs);
		createDynamicArray(inputs,lPositionMap);
		processDynamicArray(inputs,lPositionMap);
		
	}

	private static void processDynamicArray(Integer[][] inputs, Map<String, Integer> lPositionMap) {
		// TODO Auto-generated method stub
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		for(int i=lPositionMap.get("MAX_TOP");i<=lPositionMap.get("MAX_DOWN");i++)
		{
			for(int j=lPositionMap.get("MAX_LEFT");j<=lPositionMap.get("MAX_RIGHT");j++)
			{
				System.out.print(inputs[i][j]+"\t");
			}
			System.out.println();
		}
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		System.out.println("SUM IS ="+lPositionMap.get("MAX_SUM"));
	}

	private static void createDynamicArray(Integer[][] inputs, Map<String, Integer> lPositionMap) {
		Integer []partialInputs=new Integer[inputs.length];
		for(int i=0;i<inputs[0].length;i++)
		{
			for(int j=0;j<inputs.length;j++)
			{
				partialInputs[j]=0;
			}
			for(int j=i;j<inputs[0].length;j++)
			{
				for(int k=0;k<inputs.length;k++)
				{
					partialInputs[k]=partialInputs[k]+inputs[k][j];
				}
				Integer[] KadanesAlgorithmOutput = kadensAlgorithm(lPositionMap, partialInputs, i, j);
			}
			
		}
	}

	private static Integer[] kadensAlgorithm(Map<String, Integer> lPositionMap, Integer[] partialInputs, int i, int j) {
		Integer []KadanesAlgorithmOutput=KadanesAlgorithm.createDynamicArray(partialInputs);
		Integer maxSum=KadanesAlgorithm.getMaxSumValue(KadanesAlgorithmOutput);
		Integer[] startEndPoint=KadanesAlgorithm.processDynaicArray(KadanesAlgorithmOutput, partialInputs);
		lPositionMap.put("CURR_SUM", maxSum);
		lPositionMap.put("LEFT",i );
		lPositionMap.put("RIGHT", j);
		lPositionMap.put("TOP", startEndPoint[0]);
		lPositionMap.put("DOWN", startEndPoint[1]);
		if(lPositionMap.get("CURR_SUM")>=lPositionMap.get("MAX_SUM"))
		{
			lPositionMap.put("MAX_SUM", lPositionMap.get("CURR_SUM"));
			lPositionMap.put("MAX_LEFT", lPositionMap.get("LEFT"));
			lPositionMap.put("MAX_RIGHT",lPositionMap.get("RIGHT"));
			lPositionMap.put("MAX_TOP", lPositionMap.get("TOP"));
			lPositionMap.put("MAX_DOWN", lPositionMap.get("DOWN"));
		}
		return KadanesAlgorithmOutput;
	}
	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");

		}
		System.out.println("");
	}
	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
