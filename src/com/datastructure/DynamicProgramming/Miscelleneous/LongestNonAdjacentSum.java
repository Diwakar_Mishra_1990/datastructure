package com.datastructure.DynamicProgramming.Miscelleneous;

public class LongestNonAdjacentSum {
public static void main(String[] args) {
	
	Integer []inputs= new Integer[]{4,1,1,4,2,1};
	createAndProcessDynamicArray(inputs);
}

private static void createAndProcessDynamicArray(Integer[] inputs) {
	// TODO Auto-generated method stub
	int inclusive=0;
	int exclusive=0;
	int sum=0;
	for(int i=0;i<inputs.length;i++)
	{
		if(inclusive<exclusive+inputs[i])
		{
			inclusive=exclusive+inputs[i];
			sum=inclusive;
		}
		else
		{
			exclusive=inclusive;
		}
	}
	System.out.println("THE NON ADJACENT SUM IS :"+sum);
}
}
