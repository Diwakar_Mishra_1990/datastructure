package com.datastructure.DynamicProgramming.Miscelleneous;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoxStackingProblem {

	public static void main(String[] args) {
		String boxDimension="1,2,4\n3,2,5";
		List<Box> inputBox=processInputs(boxDimension);
		Collections.sort(inputBox);
		Integer[] heightStack=new Integer[inputBox.size()];
		Integer[] boxStack=new Integer[inputBox.size()];
		processInputBox(inputBox,heightStack,boxStack);
		printList(inputBox);
		printArray(heightStack);
		printArray(boxStack);
		processDynamicArray(inputBox,heightStack,boxStack);
		}

	private static void processDynamicArray(List<Box> inputBox, Integer[] heightStack, Integer[] boxStack) {
		// TODO Auto-generated method stub
		int max=0;
		int pos=0;
		for(int i=0;i<heightStack.length;i++)
		{
			if(heightStack[i]>max)
			{
				max=heightStack[i];
				pos=i;
			}
		}
		int i=pos;
		List <Box> lStackBox=new ArrayList<Box>();
		lStackBox.add(inputBox.get(i));
		while(i>0)
		{
			i=boxStack[i];
			lStackBox.add(inputBox.get(i));
		}
		System.out.println("THE MAXIMUM HEIGHT ATTAINED IS :"+max);
		System.out.println("THE STACK BEING OF THE ORDER");
		printList(lStackBox);
	}

	private static void processInputBox(List<Box> inputBox, Integer[] heightStack, Integer[] boxStack) {
		// TODO Auto-generated method stub
		int k=0;
		for(Box b:inputBox)
		{
			heightStack[k]=b.getHeight();
			boxStack[k]=k;
			k++;
		}
		for(int i=1;i<inputBox.size();i++)
		{
			Box topBox=inputBox.get(i);
			for(int j=0;j<i;j++)
			{
				Box baseBox=inputBox.get(j);
				if(topBox.getLength()<baseBox.getLength() && topBox.getBreadth()<baseBox.getBreadth())
				{
					if(heightStack[j]+topBox.getHeight()>heightStack[i])
					{
						heightStack[i]=heightStack[j]+topBox.getHeight();
						boxStack[i]=j;
					}
				}
			}
		}
	}

	private static void printList(List<Box> inputBox) {
		for(Box b:inputBox)
		{
			System.out.println(b);
		}
		
	}

	private static List<Box> processInputs(String boxDimension) {
		// TODO Auto-generated method stub
		List<Box> inputBox=new ArrayList<Box>();
		String []boxes=boxDimension.split("\n");
		for(int i=0;i<boxes.length;i++)
		{
			Box lBox=new Box();
			String []boxDimensions=boxes[i].split(",");
			if(Integer.parseInt(boxDimensions[1])>Integer.parseInt(boxDimensions[0]))
			{
				lBox.setBreadth(Integer.parseInt(boxDimensions[0]));
				lBox.setLength(Integer.parseInt(boxDimensions[1]));
				lBox.setHeight(Integer.parseInt(boxDimensions[2]));
			}
			else
			{
				lBox.setLength(Integer.parseInt(boxDimensions[0]));
				lBox.setBreadth(Integer.parseInt(boxDimensions[1]));
				lBox.setHeight(Integer.parseInt(boxDimensions[2]));
			}
			inputBox.add(lBox);
			lBox=new Box();
			if(Integer.parseInt(boxDimensions[0])>Integer.parseInt(boxDimensions[2]))
			{
				lBox.setBreadth(Integer.parseInt(boxDimensions[2]));
				lBox.setLength(Integer.parseInt(boxDimensions[0]));
				lBox.setHeight(Integer.parseInt(boxDimensions[1]));
			}
			else
			{
				lBox.setLength(Integer.parseInt(boxDimensions[2]));
				lBox.setBreadth(Integer.parseInt(boxDimensions[0]));
				lBox.setHeight(Integer.parseInt(boxDimensions[1]));
			}
			inputBox.add(lBox);
			lBox=new Box();
			if(Integer.parseInt(boxDimensions[2])>Integer.parseInt(boxDimensions[1]))
			{
				lBox.setBreadth(Integer.parseInt(boxDimensions[1]));
				lBox.setLength(Integer.parseInt(boxDimensions[2]));
				lBox.setHeight(Integer.parseInt(boxDimensions[0]));
			}
			else
			{
				lBox.setLength(Integer.parseInt(boxDimensions[1]));
				lBox.setBreadth(Integer.parseInt(boxDimensions[2]));
				lBox.setHeight(Integer.parseInt(boxDimensions[0]));
			}
			inputBox.add(lBox);
		}
		return inputBox;
	}
	private static void printArray(Integer[] inputs) {

		for (int i = 0; i < inputs.length; i++) {
				System.out.print(inputs[i] + "\t");
		}
		System.out.println();
		// TODO Auto-generated method stub
		
	}
}

class Box implements Comparable<Box>
{
	int length;
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getBreadth() {
		return breadth;
	}
	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	int breadth;
	int height;
	@Override
	public int compareTo(Box o) {
		//Sorting in descending order
		if(this.length*this.breadth>o.length*o.breadth)
		{
			return -1;
		}
		else if(this.length*this.breadth<o.length*o.breadth)
		{
			return 1;
		}
		return 0;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Length :"+this.length+", Breadth :"+this.breadth+", Height :"+this.height;
	}
}
