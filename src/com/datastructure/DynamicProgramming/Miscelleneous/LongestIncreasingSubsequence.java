package com.datastructure.DynamicProgramming.Miscelleneous;

import java.util.Set;
import java.util.TreeSet;

public class LongestIncreasingSubsequence {
	public static void main(String[] args) {
		Integer[] inputs = new Integer[] { 3, 4, -1, 0, 6, 2, 3 };
		printArray(inputs);
		Integer[] lDynamicAray = createSingleDimensionDynamicArray(inputs);
		processdynamicArray(lDynamicAray);
		printArray(lDynamicAray);
	}

	public static void processdynamicArray(Integer[] lDynamicArray) {
		// TODO Auto-generated method stub
		int maximum=0;
		for(int i=0;i<lDynamicArray.length;i++)
		{
			maximum=Math.max(maximum, lDynamicArray[i]);
		}
		System.out.println("MAXIMUM LONGEST SUBSEQUENCE IS");
		System.out.println(maximum);
	}

	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");

		}
		System.out.println("");
	}

	public static Integer[] createSingleDimensionDynamicArray(Integer[] inputs) {
		// TODO Auto-generated method stub
		Integer[] minimumJumpArrayDynamicMap = new Integer[inputs.length];
		for (int i = 0; i < inputs.length; i++) {
			minimumJumpArrayDynamicMap[i] = 1;
		}
		for (int i = 1; i < inputs.length; i++) {
			minimumJumpArrayDynamicMap[i] = geMaximumSubsequence(minimumJumpArrayDynamicMap,inputs, i);
		}
		return minimumJumpArrayDynamicMap;
	}

	private static Integer geMaximumSubsequence(Integer[] minimumJumpArrayDynamicMap, Integer[] inputs, int endIndex) {
		// TODO Auto-generated method stub
		int maximum = 1;
		for (int i = 0; i < endIndex; i++) {
			if (inputs[i] < inputs[endIndex]) {
				maximum = Math.max(maximum, minimumJumpArrayDynamicMap[i] + 1);
			}
		}
		return maximum;
	}
}
