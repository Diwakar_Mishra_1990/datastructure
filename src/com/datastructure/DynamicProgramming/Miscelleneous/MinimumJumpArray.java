package com.datastructure.DynamicProgramming.Miscelleneous;

import java.util.Set;
import java.util.TreeSet;

public class MinimumJumpArray {
	public static void main(String[] args) {
		Integer[] inputs = new Integer[] { 2, 3, 1, 1, 2, 4, 2, 0, 1, 1 };
		Integer[] jumpPosition = new Integer[inputs.length];
		Integer[] lDynamicAray = createSingleDimensionDynamicArray(inputs, jumpPosition);
		processdynamicArray(jumpPosition,lDynamicAray);
		printArray(lDynamicAray);
		printArray(jumpPosition);
	}

	private static void processdynamicArray(Integer[] jumpPosition, Integer[] lDynamicAray) {
		// TODO Auto-generated method stub
		Set<Integer> jumpPattern=new TreeSet<Integer>();
		for(int i=0;i<jumpPosition.length;i++)
		{
		jumpPattern.add(jumpPosition[i]);
		}
		System.out.println("MINIMUM NO OF JUMP IS");
		System.out.println(lDynamicAray[lDynamicAray.length-1]);
		System.out.println("The JUMP PATTERN IS");
		System.out.println(jumpPattern);
	}

	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");

		}
		System.out.println("");
	}

	private static Integer[] createSingleDimensionDynamicArray(Integer[] inputs, Integer[] jumpPosition) {
		// TODO Auto-generated method stub
		Integer[] minimumJumpArrayDynamicMap = new Integer[inputs.length];
		minimumJumpArrayDynamicMap[0] = 0;
		jumpPosition[0] = 0;
		for (int i = 1; i < inputs.length; i++) {
			minimumJumpArrayDynamicMap[i] = getMinimumJump(minimumJumpArrayDynamicMap, jumpPosition, inputs, i);
		}
		return minimumJumpArrayDynamicMap;
	}

	private static Integer getMinimumJump(Integer[] minimumJumpArrayDynamicMap, Integer[] jumpList, Integer[] inputs,
			int endIndex) {
		// TODO Auto-generated method stub
		int minimum = Integer.MAX_VALUE;
		for (int i = 0; i < endIndex; i++) {
			if (inputs[i] + i >= endIndex) {
				int tempMin = minimum;
				minimum = Math.min(minimum, minimumJumpArrayDynamicMap[i] + 1);
				if (minimum != tempMin) {
					jumpList[endIndex] = i;
				}
			}
		}
		return minimum;
	}
}
