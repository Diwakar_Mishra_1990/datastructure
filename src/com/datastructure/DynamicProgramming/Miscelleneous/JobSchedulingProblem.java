package com.datastructure.DynamicProgramming.Miscelleneous;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class JobSchedulingProblem {

	public static void main(String[] args) {
		String inputs="4,6,5\n6,7,4\n1,3,5\n7,9,2\n2,5,6\n5,8,11";
		List<JobSchedule> lJobList = processInputs(inputs);
		Integer[] lDynamicAray = createSingleDimensionDynamicArray(lJobList);
		processdynamicArray(lDynamicAray);
		printArray(lDynamicAray);
	}

	private static Integer[] createSingleDimensionDynamicArray(List<JobSchedule> lJobList) {
		// TODO Auto-generated method stub
		Integer []lDynamicArrayMap=new Integer[lJobList.size()];
		for(int i=0;i<lDynamicArrayMap.length;i++)
		{
			lDynamicArrayMap[i]=lJobList.get(i).getValue();
		}
		for(int i=1;i<lDynamicArrayMap.length;i++)
		{
			lDynamicArrayMap[i]=getMaximumValue(lJobList,lDynamicArrayMap,i);
		}
		return lDynamicArrayMap;
	}

	private static Integer getMaximumValue(List<JobSchedule> lJobList, Integer[] lDynamicArrayMap, int endIndex) {
		int maximum= lDynamicArrayMap[endIndex];
		for(int i=0;i<=endIndex;i++)
		{
			if(lJobList.get(i).getEndTime()<=lJobList.get(endIndex).getStartTime())
			{
				maximum=Math.max(maximum, lDynamicArrayMap[i]+lJobList.get(endIndex).getValue());
			}
		}
		return maximum;
	}

	private static List<JobSchedule> processInputs(String inputs) {
		String linputs[]=inputs.split("\n");
		List<JobSchedule> lJobList=new ArrayList<JobSchedule>();
		for(int i=0;i<linputs.length;i++)
		{
		String[] lJob=linputs[i].split(",");
		JobSchedule lNewJob=new JobSchedule();
		lNewJob.setStartTime(Integer.parseInt(lJob[0]));
		lNewJob.setEndTime(Integer.parseInt(lJob[1]));
		lNewJob.setValue(Integer.parseInt(lJob[2]));
		lJobList.add(lNewJob);
		}
		Collections.sort(lJobList);
		System.out.println(lJobList);
		return lJobList;
	}

	private static void processdynamicArray(Integer[] lDynamicArray) {
		// TODO Auto-generated method stub
		int maximum=0;
		for(int i=0;i<lDynamicArray.length;i++)
		{
			maximum=Math.max(maximum, lDynamicArray[i]);
		}
		System.out.println("MAXIMUM VALUE IS");
		System.out.println(maximum);
	}

	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");

		}
		System.out.println("");
	}


}

class JobSchedule  implements Comparable<JobSchedule>{
	int startTime;
	public int getStartTime() {
		return startTime;
	}
	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}
	public int getEndTime() {
		return endTime;
	}
	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	int endTime;
	int value;
	@Override
	public int compareTo(JobSchedule o) {
		// TODO Auto-generated method stub
		if(o.getEndTime()>this.endTime)
		{
			return -1;
		}
		else if(o.getEndTime()<this.endTime)
		{
			return 1;
		}
		return 0;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "("+this.startTime+","+this.endTime+")";
	}
}
