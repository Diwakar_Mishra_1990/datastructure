package com.datastructure.DynamicProgramming.Miscelleneous;

public class LongestBitonicSubSequence {

	public static void main(String[] args) {
		Integer[] inputs = new Integer[] { 3, 4, -1, 0, 6, 2, 3 };
		printArray(inputs);
		Integer[] lDynamicAray1 = LongestIncreasingSubsequence.createSingleDimensionDynamicArray(inputs);
		printArray(lDynamicAray1);
		inputs = reverseArray(inputs);
		System.out.println("Rev Array");
		printArray(inputs);
		Integer[] lDynamicAray2 = LongestIncreasingSubsequence.createSingleDimensionDynamicArray(inputs);
		printArray(lDynamicAray1);
		processdynamicArray(lDynamicAray1,lDynamicAray2);
		
	}
	private static void processdynamicArray(Integer[] lDynamicAray1, Integer[] lDynamicAray2) {
		// TODO Auto-generated method stub
		Integer []lDynamicArray= new Integer[lDynamicAray1.length];
		for(int i=0;i<lDynamicArray.length;i++)
		{
			lDynamicArray[i]=lDynamicAray1[i]+lDynamicAray2[i]-1;
		}
		LongestIncreasingSubsequence.processdynamicArray(lDynamicArray);
	}
	private static Integer[] reverseArray(Integer[] inputs) {
		for(int i=inputs.length-1;i>=(inputs.length)/2;i--)
		{
			int temp=inputs[i];
			inputs[i]=inputs[inputs.length-1-i];
			inputs[inputs.length-1-i]=temp;
		}
		return inputs;
	}
	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");

		}
		System.out.println("");
	}
}
