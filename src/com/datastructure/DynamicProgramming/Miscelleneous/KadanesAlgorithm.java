package com.datastructure.DynamicProgramming.Miscelleneous;


public class KadanesAlgorithm {
	public static void main(String[] args) {
		Integer[] inputs = new Integer[] { -2, -3, 1, 1 };
		Integer[] lDynamicArrayMap = createDynamicArray(inputs);
		printArray(inputs);
		printArray(lDynamicArrayMap);
		Integer[] lDataPoints = processDynaicArray(lDynamicArrayMap,inputs);
		printArray(lDataPoints);
	}

	public static Integer[] processDynaicArray(Integer[] lDynamicArrayMap, Integer[] inputs) {
		// TODO Auto-generated method stub
		Integer[] startEndPos=new Integer[2];
		Integer position = getMaxSum(lDynamicArrayMap);
		startEndPos[1]=position;
		int i=position;
		int sub=lDynamicArrayMap[position];
		while(i!=0)
		{
			sub=sub-inputs[i];
			if(inputs[i]!=0 && sub==0)
			{
				break;
			}
			i--;
		}
		startEndPos[0]=i;
		return startEndPos;
	}
	public static Integer getMaxSumValue(Integer[] lDynamicArrayMap) {

		// TODO Auto-generated method stub
		int max = lDynamicArrayMap[lDynamicArrayMap.length - 1];
		int pos = lDynamicArrayMap.length - 1;
		for (int i = lDynamicArrayMap.length - 1; i >= 0; i--) {
			if (lDynamicArrayMap[i] > max) {
				max = lDynamicArrayMap[i];
				pos = i;
			}
		}
		return max;
	}

	private static Integer getMaxSum(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		int max = lDynamicArrayMap[lDynamicArrayMap.length - 1];
		int pos = lDynamicArrayMap.length - 1;
		for (int i = lDynamicArrayMap.length - 1; i >= 0; i--) {
			if (lDynamicArrayMap[i] > max) {
				max = lDynamicArrayMap[i];
				pos = i;
			}
		}
		return pos;
	}

	public static Integer[] createDynamicArray(Integer[] inputs) {
		// TODO Auto-generated method stub
		Integer[] lDynamicArrayMap = new Integer[inputs.length];
		lDynamicArrayMap[0] = inputs[0];
		for (int i = 1; i < lDynamicArrayMap.length; i++) {
			lDynamicArrayMap[i] = getMaximumSum(inputs, lDynamicArrayMap, i);
		}
		return lDynamicArrayMap;
	}

	private static void printArray(Integer[] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			System.out.print(lDynamicArrayMap[i] + "\t");
		}
		System.out.println("");
	}

	public static Integer getMaximumSum(Integer[] inputs, Integer[] lDynamicArrayMap, int endIndex) {
		// TODO Auto-generated method stub
		int maximum = Integer.MIN_VALUE;
		int temp = maximum;
		int start = 0;
		int sum = 0;
		for (int i = endIndex; i >= 0; i--) {
			sum = sum + inputs[i];
			maximum = Math.max(maximum, sum);
			/*
			 * if(temp!=maximum) { start=i; }
			 */
		}
		lDynamicArrayMap[endIndex] = maximum;
		return maximum;
	}
}
