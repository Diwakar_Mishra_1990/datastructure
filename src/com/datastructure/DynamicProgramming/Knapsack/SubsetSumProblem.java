package com.datastructure.DynamicProgramming.Knapsack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubsetSumProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputs = "2,3,7,8,10\n11";
		List<Integer> lInputSet = new ArrayList<Integer>();
		int totalValue = processInputs(inputs, lInputSet);
		String[][] lDynamicArrayMap = createDynamicArray(totalValue, lInputSet);
		processDyanamicArray(lDynamicArrayMap, lInputSet);

	}

	private static void processDyanamicArray(String[][] lDynamicArrayMap, List<Integer> lInputSet) {
		// TODO Auto-generated method stub
		int i = lDynamicArrayMap.length - 1;
		int j = lDynamicArrayMap[i].length - 1;
		System.out.println("*************************************************");
		while (!(i==1 || j==1)) {
			if (lDynamicArrayMap[i][j] == lDynamicArrayMap[i - 1][j]) {
				i--;
			} else {
				System.out.println(lDynamicArrayMap[i][0]);
				j = j - Integer.parseInt(lDynamicArrayMap[i][0]);
				i--;
			}
		}
	}

	private static String[][] createDynamicArray(int totalValue, List<Integer> lInputSet) {
		// TODO Auto-generated method stub
		String[] lDynamicArrayMap[] = new String[lInputSet.size() + 2][totalValue + 2];
		List<Integer> lInputList = lInputSet;
		Collections.sort(lInputList);
		lDynamicArrayMap[1][0] = 0 + "";
		// Let us assume 0 being false
		// and 1 being true
		for (int i = 1; i < lDynamicArrayMap[0].length; i++) {
			lDynamicArrayMap[0][i] = i - 1 + "";
			lDynamicArrayMap[1][i] = "F";
		}
		lDynamicArrayMap[1][1] = "T";
		for (int j = 2; j < lDynamicArrayMap.length; j++) {

			lDynamicArrayMap[j][0] = lInputList.get(j - 2) + "";
			lDynamicArrayMap[j][1] = "T";
		}
		for (int i = 2; i < lDynamicArrayMap.length; i++) {
			for (int j = 2; j < lDynamicArrayMap[i].length; j++) {
				int maxWeight = Integer.parseInt(lDynamicArrayMap[0][j]);
				int itemWeight = Integer.parseInt(lDynamicArrayMap[i][0]);
				if (itemWeight <= maxWeight) {
					/**
					 * This is the knap sack algorithm a slight change with
					 * basic motive being the same we are able to achieve our
					 * results
					 */
					// lDynamicArrayMap[i][j]=Math.max(lDynamicArrayMap[i-1][j],
					// lInputSet.get(itemWeight)+lDynamicArrayMap[i-1][(j-1)-lDynamicArrayMap[0][i]]);
					if (lDynamicArrayMap[i - 1][j].equals("T")) {
						lDynamicArrayMap[i][j] = lDynamicArrayMap[i - 1][j];
					} else {
						lDynamicArrayMap[i][j] = lDynamicArrayMap[i - 1][j - Integer.parseInt(lDynamicArrayMap[i][0])];
					}
				} else {
					lDynamicArrayMap[i][j] = lDynamicArrayMap[i - 1][j];
				}
			}
		}
		printArray(lDynamicArrayMap);
		return lDynamicArrayMap;
	}

	private static void printArray(String[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}

	private static int processInputs(String inputs, List<Integer> lInputSet) {
		// TODO Auto-generated method stub
		String[] inputsCombo = inputs.split("\n");
		String[] lInputs = inputsCombo[0].split(",");
		for (int i = 0; i < lInputs.length; i++) {
			lInputSet.add(Integer.parseInt(lInputs[i]));
		}
		System.out.println(lInputSet);
		return Integer.parseInt(inputsCombo[inputsCombo.length - 1]);
	}

}
