package com.datastructure.DynamicProgramming.Knapsack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EggDroppingProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputs = "6\n2";
		Map<Integer, Integer> lWeightValueMaps = new HashMap<Integer, Integer>();
		String[] inputsCombo = inputs.split("\n");
		int totalFloors = Integer.parseInt(inputsCombo[0]);
		int noOfEggs = Integer.parseInt(inputsCombo[1]);
		Integer[] lDynamicArrayMap[] = createDynamicArray(totalFloors, noOfEggs);
		// processDyanamicArray(lDynamicArrayMap,lWeightValueMaps);

	}

	private static void processDyanamicArray(Integer[][] lDynamicArrayMap, Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		int i = lDynamicArrayMap.length - 1;
		int j = lDynamicArrayMap[i].length - 1;
		System.out.println("*************************************************");
		while (lDynamicArrayMap[i][j] != 0) {
			if (lDynamicArrayMap[i][j] == lDynamicArrayMap[i - 1][j]) {
				i--;
			} else if (lDynamicArrayMap[i][j] != 0) {
				System.out.println(lDynamicArrayMap[i][0]);
				j = j - lDynamicArrayMap[i][0];
				i--;
			}
		}
	}

	private static Integer[][] createDynamicArray(int totalValue, int noOfEggs) {
		// TODO Auto-generated method stub
		Integer[] lDynamicArrayMap[] = new Integer[noOfEggs + 1][totalValue + 1];
		for (int i = 1; i < lDynamicArrayMap[0].length; i++) {
			lDynamicArrayMap[0][i] = i;
			lDynamicArrayMap[1][i] = i;
		}
		for (int j = 1; j < lDynamicArrayMap.length; j++) {
			lDynamicArrayMap[j][0] = j;
		}
		for (int i = 2; i < lDynamicArrayMap.length; i++) {
			for (int j = 1; j < lDynamicArrayMap[i].length; j++) {
				int floor = lDynamicArrayMap[0][j];
				int eggNo = lDynamicArrayMap[i][0];
				if (eggNo <= floor) {
					/**
					 * Biggest digression from knapsack
					 */
					// lDynamicArrayMap[i][j]=Math.max(lDynamicArrayMap[i-1][j],
					// noOfEggs.get(eggNo)+lDynamicArrayMap[i-1][j-lDynamicArrayMap[i][0]]);
					lDynamicArrayMap[i][j] = getMinimumForRoot(lDynamicArrayMap,i, j);
				} else {
					lDynamicArrayMap[i][j] = lDynamicArrayMap[i - 1][j];
				}
			}
		}
		printArray(lDynamicArrayMap);
		return lDynamicArrayMap;
	}

	/**
	 * In this function you will note that +1 is added to every input +1 takes
	 * care that my leftmost is an input and topmost is also an input and not
	 * involved in computation
	 * 
	 * @param lDynamicArrayMap
	 * @return
	 */
	private static int getMinimumForRoot(Integer[][] lDynamicArrayMap, int startIndex, int endIndex) {
		int minimum = Integer.MAX_VALUE;
		int temp = minimum;
		for (int j = 1; j <= endIndex; j++) {
			minimum = Math.min(minimum,
					Math.max(lDynamicArrayMap[startIndex - 1][j - 1], lDynamicArrayMap[startIndex][endIndex - j]));
		}
		return minimum;
	}

	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}

	private static int processInputs(String inputs, Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		String[] inputsCombo = inputs.split("\n");
		String[] inputsWeight = inputsCombo[0].split(",");
		String[] inputsValue = inputsCombo[1].split(",");
		for (int i = 0; i < inputsWeight.length; i++) {
			lWeightValueMaps.put(Integer.parseInt(inputsWeight[i]), Integer.parseInt(inputsValue[i]));
		}
		System.out.println(lWeightValueMaps);
		return Integer.parseInt(inputsCombo[inputsCombo.length - 1]);
	}

}
