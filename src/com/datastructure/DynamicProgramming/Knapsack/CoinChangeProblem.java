package com.datastructure.DynamicProgramming.Knapsack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * The problem deals with coin with denomination 1,5,6,8 available in ample amounts . We need to calculate
 * minimum no of coins taken to achieve so .
 * The algorithm is same as knapsack problem with a minor change 
 * @author Diwakar Mishra
 *
 */
public class CoinChangeProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputs = "1,5,6,8\n11";
		List<Integer> lCoinValues = new ArrayList<Integer>();
		int totalValue = processInputs(inputs, lCoinValues);
		Integer[] lDynamicArrayMap[] = createDynamicArray(totalValue, lCoinValues);
		processDyanamicArray(lDynamicArrayMap, lCoinValues);

	}

	private static void processDyanamicArray(Integer[][] lDynamicArrayMap, List<Integer> lCoinValues) {
		// TODO Auto-generated method stub
		int i = lDynamicArrayMap.length - 1;
		int j = lDynamicArrayMap[i].length - 1;
		System.out.println("*************************************************");
		while (lDynamicArrayMap[i][j] != 0) {
			if (lDynamicArrayMap[i][j] == lDynamicArrayMap[i - 1][j]) {
				i--;
			} else if (lDynamicArrayMap[i][j] != 0) {
				System.out.println(lDynamicArrayMap[i][0]);
				j = j - lDynamicArrayMap[i][0];
				i--;
			}
		}
	}

	private static Integer[][] createDynamicArray(int totalValue, List<Integer> lCoinValues) {
		// TODO Auto-generated method stub
		Integer[] lDynamicArrayMap[] = new Integer[lCoinValues.size() + 2][totalValue + 2];
		lDynamicArrayMap[1][0] = 0;
		for (int i = 1; i < lDynamicArrayMap[0].length; i++) {
			lDynamicArrayMap[0][i] = i - 1;
			//In the knapsack problem to start our problem we put the value at first row as 0
			// However as a minor change here we will put the values from 0 to 11 in first row 
			//to start our algorithm
			//lDynamicArrayMap[1][i] = 0;
			lDynamicArrayMap[1][i] = lDynamicArrayMap[0][i];
		}
		for (int j = 2; j < lDynamicArrayMap.length; j++) {

			lDynamicArrayMap[j][0] = lCoinValues.get(j - 2);
			lDynamicArrayMap[j][1] = 0;
		}
		for (int i = 2; i < lDynamicArrayMap.length; i++) {
			for (int j = 2; j < lDynamicArrayMap[i].length; j++) {
				int value = lDynamicArrayMap[0][j];
				int lDenomintion = lDynamicArrayMap[i][0];
				if (lDenomintion <= value) {
					/*
					 * Small change in algorithm . Here we know what denomination we are 
						using . Our primary aim is to find the no of coins required . so if
						our coin is involved in the process the value added will be "1"
						
						2. Since we are calculating minimum no of coins so here min function will be
						used instead of max
						
					*/
					
					
					/*lDynamicArrayMap[i][j] = Math.max(lDynamicArrayMap[i - 1][j],
							lDenomintion + lDynamicArrayMap[i - 1][j - lDynamicArrayMap[i][0]]);*/
					
					lDynamicArrayMap[i][j] = Math.min(lDynamicArrayMap[i - 1][j],
							1 + lDynamicArrayMap[i - 1][j - lDynamicArrayMap[i][0]]);
				} else {
					lDynamicArrayMap[i][j] = lDynamicArrayMap[i - 1][j];
				}
			}
		}
		printArray(lDynamicArrayMap);
		return lDynamicArrayMap;
	}

	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}

	private static int processInputs(String inputs, List<Integer> lCoinValues) {
		// TODO Auto-generated method stub
		String[] inputsCombo = inputs.split("\n");
		String[] inputsCoins = inputsCombo[0].split(",");
		for (int i = 0; i < inputsCoins.length; i++) {
			lCoinValues.add(Integer.parseInt(inputsCoins[i]));
		}
		Collections.sort(lCoinValues);
		System.out.println(lCoinValues);
		return Integer.parseInt(inputsCombo[inputsCombo.length - 1]);
	}

}
