package com.datastructure.DynamicProgramming.Knapsack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KnapSackProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputs="1,3,4,5\n1,4,5,7\n7";
		Map<Integer,Integer> lWeightValueMaps=new HashMap<Integer,Integer>();
		int totalValue=processInputs(inputs,lWeightValueMaps);
		Integer []lDynamicArrayMap[]= createDynamicArray(totalValue,lWeightValueMaps);
		processDyanamicArray(lDynamicArrayMap,lWeightValueMaps);
		
	}

	private static void processDyanamicArray(Integer[][] lDynamicArrayMap,
			Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		int i=lDynamicArrayMap.length-1;
		int j=lDynamicArrayMap[i].length-1;
		System.out.println("*************************************************");
		while(lDynamicArrayMap[i][j]!=0)
		{
			if(lDynamicArrayMap[i][j]==lDynamicArrayMap[i-1][j])
			{
				i--;
			}
			else if(lDynamicArrayMap[i][j]!=0)
			{
				System.out.println(lDynamicArrayMap[i][0]);
				j=j-lDynamicArrayMap[i][0];
				i--;
			}
		}
	}

	private static Integer[][] createDynamicArray(int totalValue, Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		Integer []lDynamicArrayMap[]=new Integer[lWeightValueMaps.keySet().size()+2][totalValue+2];
		List<Integer> lWeightsList=new ArrayList<Integer>(lWeightValueMaps.keySet());
		Collections.sort(lWeightsList);
		lDynamicArrayMap[1][0]=0;
		for(int i=1;i<lDynamicArrayMap[0].length;i++)
		{
			lDynamicArrayMap[0][i]=i-1;
			lDynamicArrayMap[1][i]=0;
		}
		for(int j=2;j<lDynamicArrayMap.length;j++)
		{
			
				lDynamicArrayMap[j][0]=lWeightsList.get(j-2);
				lDynamicArrayMap[j][1]=0;
		}
		for(int i=2;i<lDynamicArrayMap.length;i++)
		{
			for(int j=2;j<lDynamicArrayMap[i].length;j++)
			{
			    int maxWeight=lDynamicArrayMap[0][j];
			    int itemWeight=lDynamicArrayMap[i][0];
			    if(itemWeight<=maxWeight)
			    {
			    	lDynamicArrayMap[i][j]=Math.max(lDynamicArrayMap[i-1][j], lWeightValueMaps.get(itemWeight)+lDynamicArrayMap[i-1][j-lDynamicArrayMap[i][0]]);
			    }
			    else
			    {
			    	lDynamicArrayMap[i][j]=lDynamicArrayMap[i-1][j];
			    }
			}
		}
		printArray(lDynamicArrayMap);
		return lDynamicArrayMap;
	}

	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for(int i=0;i<lDynamicArrayMap.length;i++)
		{
			for(int j=0;j<lDynamicArrayMap[i].length;j++)
			{
				System.out.print(lDynamicArrayMap[i][j]+"\t");
			}
			System.out.println("");
		}
	}

	private static int processInputs(String inputs, Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		String[]inputsCombo=inputs.split("\n");
		String []inputsWeight=inputsCombo[0].split(",");
		String []inputsValue=inputsCombo[1].split(",");
		for(int i=0;i<inputsWeight.length;i++)
		{
			lWeightValueMaps.put(Integer.parseInt(inputsWeight[i]), Integer.parseInt(inputsValue[i]));
		}
		System.out.println(lWeightValueMaps);
		return Integer.parseInt(inputsCombo[inputsCombo.length-1]);
	}

}
