package com.datastructure.DynamicProgramming.SequencingAlgorithm;

public class RangeMinimum {
public static void main(String[] args) {
	Integer []inputs=new Integer[]{4,6,1,5,7,3};
	printArray(inputs);
	Integer[][] lRootMap =new Integer[inputs.length+1][inputs.length+1];
	Integer[][] lDynamicArray=createDynamicArray(inputs,lRootMap);
	
}

private static void printArray(Integer[] inputs) {

	for (int i = 0; i < inputs.length; i++) {
			System.out.print(inputs[i] + "\t");
	}
	System.out.println();
	// TODO Auto-generated method stub
	
}

private static Integer[][] createDynamicArray(Integer[] inputs, Integer[][] lRootMap) {
	Integer[][] lDynamicArray =new Integer[inputs.length+1][inputs.length+1];
	for(int i=1;i<lDynamicArray.length;i++)
	{
		lDynamicArray[0][i]=i-1;
		lDynamicArray[i][0]=i-1;
		lDynamicArray[i][i]=inputs[i-1];
		
		lRootMap[0][i]=i-1;
		lRootMap[i][0]=i-1;
		lRootMap[i][i]=i-1;
	}
	int i=1;
	int j=2;
	int tempRow=i;
	int tempColum=j;
	while(j<lDynamicArray.length &&i<lDynamicArray.length)
	{
		if(lDynamicArray[i][j-1]>lDynamicArray[i+1][j])
		{
			lDynamicArray[i][j]=lDynamicArray[i+1][j];
			lRootMap[i][j]=lRootMap[i+1][j];
		}
		else
		{
			lDynamicArray[i][j]=lDynamicArray[i][j-1];
			lRootMap[i][j]=lRootMap[i][j-1];
		}
		
		//This is primarily used for navigation
		i++;
		j++;
		if(j>=lDynamicArray.length)
		{
			j=tempColum+1;
			tempColum++;
			i=tempRow;
		}
	}
	System.out.println("*************************************************************");
	printArray(lDynamicArray);
	System.out.println("*************************************************************");
	printArray(lRootMap);
	return lDynamicArray;
}
private static void printArray(Integer[][] lDynamicArrayMap) {
	// TODO Auto-generated method stub
	for (int i = 0; i < lDynamicArrayMap.length; i++) {
		for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
			System.out.print(lDynamicArrayMap[i][j] + "\t");
		}
		System.out.println("");
	}
}
}
