package com.datastructure.DynamicProgramming.SequencingAlgorithm;

import java.util.ArrayList;
import java.util.List;

import com.sun.jndi.ldap.ext.StartTlsResponseImpl;

public class PalindromePartioning {


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputs = "abcbm";
		List<String> lStringList = new ArrayList<String>();
		processInputs(inputs, lStringList);
		Integer[] lDynamicArrayMap[] = createDynamicArray(lStringList);
		printArray(lDynamicArrayMap);
		processDyanamicArray(lDynamicArrayMap);

	}

	private static void processDyanamicArray(Integer[][] lDynamicArrayMap) {
		System.out.println("No Of Partition");
		System.out.println(lDynamicArrayMap[1][lDynamicArrayMap[1].length-1]);
	}

	private static Integer[][] createDynamicArray(List<String> lStringList) {
		// TODO Auto-generated method stub
		Integer[] lDynamicArrayMap[] = new Integer[lStringList.size()
				+ 1][lStringList.size() + 1];
		lDynamicArrayMap[1][0] = 0;
		for (int i = 1; i < lDynamicArrayMap[0].length; i++) {
			lDynamicArrayMap[0][i] = i - 1;
		}
		for (int j = 1; j < lDynamicArrayMap.length; j++) {
			lDynamicArrayMap[j][0] = j - 1;
			lDynamicArrayMap[j][j] = 0;
			if(j+1<lDynamicArrayMap.length)
			{
				if(lStringList.get(j-1).equals(lStringList.get((j+1)-1)))
				{
					lDynamicArrayMap[j][j+1] = 0;
				}
				else
				{
					lDynamicArrayMap[j][j+1] = 1;
				}
			}
		}
		int j = 2;
		for (int i = 1; i < lStringList.size(); i++) {
			if(isPalindrome(lStringList,i-1,i+j-1))
			{
				lDynamicArrayMap[i][i+j] = 0;
			}
			else
			{
				lDynamicArrayMap[i][i+j] = 1+getMinimumValue(lDynamicArrayMap, j, i);
			}
			if (j == lStringList.size()-1 && i == 1) {
				break;
			}
			if (i + j >= lStringList.size()) {
				i = 0;
				j++;
			}
			System.out.println("****************");
			printArray(lDynamicArrayMap);
			/*
			lDynamicArrayMap[i][i + j] = getSumOfArray(lStringList, lDynamicArrayMap[i][0],
					lDynamicArrayMap[0][i + j])
					+ getMinimumForRoot(lDynamicArrayMap, root, lDynamicArrayMap[i][0], lDynamicArrayMap[0][i + j]);
			lRootArrayMap[i][i + j] = root.get(0);
			printArray(lDynamicArrayMap);
			if (lStringList.get(i-1).equals(lStringList.get(i+j-1)))
			{
				lDynamicArrayMap[i][i + j]=lDynamicArrayMap[i+1][i + j-1]+2;
			}
			else
			{
				lDynamicArrayMap[i][i + j] = Math.max(lDynamicArrayMap[i+1][i + j], lDynamicArrayMap[i][i + j-1]);
			}
			
			

		*/}
		return lDynamicArrayMap;
	}

	private static int getMinimumValue(Integer[][] lDynamicArrayMap, int j, int i) {
		int minimum=Integer.MAX_VALUE;
		int startIndex=i;
		int endIndex=i+j;
		for(int k=1;k<=endIndex-startIndex;k++)
		{
			if(i+j<lDynamicArrayMap[0].length)
			{
				minimum=Math.min(minimum, lDynamicArrayMap[i][i+k-1]+lDynamicArrayMap[i+k][i+j]);
			}
			else
			{
				minimum=Math.min(minimum, lDynamicArrayMap[i][k]+lDynamicArrayMap[k+1][lDynamicArrayMap[i].length-1]);	
			}
		}
		return minimum;
	}

	private static boolean isPalindrome(List<String> lStringList, int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		int i=startIndex;
		int j=endIndex;
		while(i<j)
		{
			if(lStringList.get(i).equals(lStringList.get(j)))
			{
				i++;
				j--;
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * In this function you will note that +1 is added to every input +1 takes
	 * care that my leftmost is an input and topmost is also an input and not
	 * involved in computation
	 * 
	 * @param lDynamicArrayMap
	 * @param root
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	private static int getMinimumForRoot(Integer[][] lDynamicArrayMap, List<Integer> root, int startIndex,
			int endIndex) {
		int minimum = Integer.MAX_VALUE;
		int temp = minimum;
		for (int i = startIndex; i <= endIndex; i++) {
			if (i == startIndex) {
				minimum = Math.min(minimum, lDynamicArrayMap[(startIndex + 1) + 1][(endIndex + 1)]);
				if (minimum != temp) {
					root.clear();
					root.add(startIndex+1);
					temp=minimum;
				}
			} else if (i == endIndex) {
				minimum = Math.min(minimum, lDynamicArrayMap[(startIndex + 1)][(endIndex + 1) - 1]);
				if (minimum != temp) {
					root.clear();
					root.add(endIndex+1);
					temp=minimum;
				}
			} else {
				minimum = Math.min(minimum, lDynamicArrayMap[(startIndex + 1)][(i + 1) - 1]
						+ lDynamicArrayMap[(i + 1) + 1][(endIndex + 1)]);
				if (minimum != temp) {
					root.clear();
					root.add(i+1);
					temp=minimum;
				}
			}

		}
		return minimum;
	}

	/*private static Integer getSumOfArray(List<String> lStringList, 
			int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		int sum = 0;
		for (int i = startIndex; i <= endIndex; i++) {
			sum += lWeightValueMaps.get(lStringList.get(i));
		}
		return sum;
	}*/

	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}

	private static void processInputs(String inputs, List<String> lStringList) {
		// TODO Auto-generated method stub
		for (int i = 0; i < inputs.length(); i++) {
			lStringList.add(inputs.charAt(i)+"");
		}
	}


}
