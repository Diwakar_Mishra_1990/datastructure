package com.datastructure.DynamicProgramming.SequencingAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptimalBinarySearchTree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputs = "10,12,16,21\n4,2,6,3";
		Map<Integer, Integer> lWeightValueMaps = new HashMap<Integer, Integer>();
		processInputs(inputs, lWeightValueMaps);
		Integer[] lRootArrayMap[] = new Integer[lWeightValueMaps.keySet().size() + 1][lWeightValueMaps.keySet().size()
				+ 1];
		Integer[] lDynamicArrayMap[] = createDynamicArray(lWeightValueMaps, lRootArrayMap);
		printArray(lDynamicArrayMap);
		printArray(lRootArrayMap);
		// processDyanamicArray(lDynamicArrayMap,lWeightValueMaps);

	}

	private static void processDyanamicArray(Integer[][] lDynamicArrayMap, Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		int i = lDynamicArrayMap.length - 1;
		int j = lDynamicArrayMap[i].length - 1;
		System.out.println("*************************************************");
		while (lDynamicArrayMap[i][j] != 0) {
			if (lDynamicArrayMap[i][j] == lDynamicArrayMap[i - 1][j]) {
				i--;
			} else if (lDynamicArrayMap[i][j] != 0) {
				System.out.println(lDynamicArrayMap[i][0]);
				j = j - lDynamicArrayMap[i][0];
				i--;
			}
		}
	}

	private static Integer[][] createDynamicArray(Map<Integer, Integer> lWeightValueMaps, Integer[][] lRootArrayMap) {
		// TODO Auto-generated method stub
		Integer[] lDynamicArrayMap[] = new Integer[lWeightValueMaps.keySet().size()
				+ 1][lWeightValueMaps.keySet().size() + 1];
		List<Integer> lWeightsList = new ArrayList<Integer>(lWeightValueMaps.keySet());
		Collections.sort(lWeightsList);
		lDynamicArrayMap[1][0] = 0;
		for (int i = 1; i < lDynamicArrayMap[0].length; i++) {
			lDynamicArrayMap[0][i] = i - 1;
		}
		for (int j = 1; j < lDynamicArrayMap.length; j++) {
			lDynamicArrayMap[j][0] = j - 1;
		}
		for (int j = 1; j < lDynamicArrayMap.length; j++) {
			lDynamicArrayMap[j][j] = lWeightValueMaps.get(lWeightsList.get(j - 1));
		}
		int j = 1;
		for (int i = 1; i <= 3; i++) {
			List<Integer> root = new ArrayList<Integer>();
			lDynamicArrayMap[i][i + j] = getSumOfArray(lWeightsList, lWeightValueMaps, lDynamicArrayMap[i][0],
					lDynamicArrayMap[0][i + j])
					+ getMinimumForRoot(lDynamicArrayMap, root, lDynamicArrayMap[i][0], lDynamicArrayMap[0][i + j]);
			lRootArrayMap[i][i + j] = root.get(0);
			if (j == 3 && i == 1) {
				break;
			}
			if (i + j > 3) {
				i = 0;
				j++;
			}

		}
		return lDynamicArrayMap;
	}

	/**
	 * In this function you will note that +1 is added to every input +1 takes
	 * care that my leftmost is an input and topmost is also an input and not
	 * involved in computation
	 * 
	 * @param lDynamicArrayMap
	 * @param root
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	private static int getMinimumForRoot(Integer[][] lDynamicArrayMap, List<Integer> root, int startIndex,
			int endIndex) {
		int minimum = Integer.MAX_VALUE;
		int temp = minimum;
		for (int i = startIndex; i <= endIndex; i++) {
			if (i == startIndex) {
				minimum = Math.min(minimum, lDynamicArrayMap[(startIndex + 1) + 1][(endIndex + 1)]);
				if (minimum != temp) {
					root.clear();
					root.add(startIndex+1);
					temp=minimum;
				}
			} else if (i == endIndex) {
				minimum = Math.min(minimum, lDynamicArrayMap[(startIndex + 1)][(endIndex + 1) - 1]);
				if (minimum != temp) {
					root.clear();
					root.add(endIndex+1);
					temp=minimum;
				}
			} else {
				minimum = Math.min(minimum, lDynamicArrayMap[(startIndex + 1)][(i + 1) - 1]
						+ lDynamicArrayMap[(i + 1) + 1][(endIndex + 1)]);
				if (minimum != temp) {
					root.clear();
					root.add(i+1);
					temp=minimum;
				}
			}

		}
		return minimum;
	}

	private static Integer getSumOfArray(List<Integer> lWeightsList, Map<Integer, Integer> lWeightValueMaps,
			int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		int sum = 0;
		for (int i = startIndex; i <= endIndex; i++) {
			sum += lWeightValueMaps.get(lWeightsList.get(i));
		}
		return sum;
	}

	private static void printArray(Integer[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}

	private static void processInputs(String inputs, Map<Integer, Integer> lWeightValueMaps) {
		// TODO Auto-generated method stub
		String[] inputsCombo = inputs.split("\n");
		String[] inputsWeight = inputsCombo[0].split(",");
		String[] inputsValue = inputsCombo[1].split(",");
		for (int i = 0; i < inputsWeight.length; i++) {
			lWeightValueMaps.put(Integer.parseInt(inputsWeight[i]), Integer.parseInt(inputsValue[i]));
		}
		List<Integer> lWeightList = new ArrayList<Integer>(lWeightValueMaps.keySet());
		Collections.sort(lWeightList);
		for (int i = 0; i < lWeightList.size(); i++) {
			System.out.println(lWeightList.get(i) + "=" + lWeightValueMaps.get(lWeightList.get(i)));
		}
	}

}
