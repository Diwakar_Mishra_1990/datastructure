package com.datastructure.DynamicProgramming.SequencingAlgorithm;

import java.util.ArrayList;
import java.util.List;

public class NoOfWords {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> dictionary = createDictionary();
		String inputs="Iamanace";
		String[][]lDynamicArrayMap=createDynamicArrayMap(inputs,dictionary);
		printArray(lDynamicArrayMap);
		processDynamicArray(lDynamicArrayMap);
	}
	private static void processDynamicArray(String[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		if(lDynamicArrayMap[1][lDynamicArrayMap[1].length-1].equals("T"))
		{
			System.out.println("IT CONTAINS VALID WORDS");
		}
		else
		{
			System.out.println("IT CONTAINS NO VALID WORDS");
		}
	}
	private static void printArray(Integer[] inputs) {

		for (int i = 0; i < inputs.length; i++) {
				System.out.print(inputs[i] + "\t");
		}
		System.out.println();
		// TODO Auto-generated method stub
		
	}
	private static void printArray(String[][] lDynamicArrayMap) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lDynamicArrayMap.length; i++) {
			for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
				System.out.print(lDynamicArrayMap[i][j] + "\t");
			}
			System.out.println("");
		}
	}
	private static String[][] createDynamicArrayMap(String inputs, List<String> dictionary) {
		// TODO Auto-generated method stub
		String[][] lDynamicArray=new String[inputs.length()+1][inputs.length()+1];
		
		for(int i=1;i<lDynamicArray.length;i++)
		{
			lDynamicArray[0][i]=i-1+"";
			lDynamicArray[i][0]=i-1+"";
		}
		for(int i=1;i<lDynamicArray.length;i++)
		{
			if(dictionary.contains(inputs.charAt(i-1)+""))
			{
				lDynamicArray[i][i]="T";
			}
			else
			{
				lDynamicArray[i][i]="F";
			}
		}
		int i=1;
		int j=2;
		int tempRow=i;
		int tempColum=j;
		while(j<lDynamicArray.length &&i<lDynamicArray.length)
		{
			if(dictionary.contains(inputs.substring(i-1, j)))
			{
				lDynamicArray[i][j]="T";
			}
			else
			{
				for(int k=i-1;k<j-1;k++)
				{
					if(getBoolenResult(lDynamicArray[(i-1)+1][k+1],lDynamicArray[(k+1)+1][(j-1)+1]))
					{
						lDynamicArray[i][j]="T";
						break;
					}
					else
					{
						lDynamicArray[i][j]="F";
					}
				}
			}
			//This is primarily used for navigation
			i++;
			j++;
			if(j>=lDynamicArray.length)
			{
				j=tempColum+1;
				tempColum++;
				i=tempRow;
			}
		}
	return lDynamicArray;
	}

	private static Boolean getBoolenResult(String string1, String string2) {
		boolean b1=false;
		boolean b2=false;
		if(string1.equals("T"))
		{
			b1=true;
		}
		if(string2.equals("T"))
		{
			b2=true;
		}
		return b1&&b2;
	}
	/**
	 * 
	 */
	private static List<String> createDictionary() {
		List<String> dictionary = new ArrayList<String>();
		dictionary.add("I");
		dictionary.add("i");
		dictionary.add("am");
		dictionary.add("AM");
		dictionary.add("a");
		dictionary.add("A");
		dictionary.add("ace");
		dictionary.add("ace");
		dictionary.add("an");
		dictionary.add("AN");
		return dictionary;
	}

}
