package com.datastructure.string;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringCombination {

	public static void main(String[] args) {
		String s = "AABC";
		String[] results = new String[s.length()];
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (int i = 0; i < s.length(); i++) {
			if (!countMap.containsKey(s.charAt(i) + "")) {
				countMap.put(s.charAt(i) + "", 1);
			} else {
				countMap.put(s.charAt(i) + "", countMap.get(s.charAt(i) + "") + 1);
			}
		}
		System.out.println(countMap);
		combinations(countMap, results,0,0);
	}

	private static void combinations(Map<String, Integer> countMap, String[] results,int level, int start) {
		// TODO Auto-generated method stub
		Map<String, Integer> lTempMap = new HashMap<String, Integer>();
		lTempMap.putAll(countMap);
		//System.out.println(lTempMap);
		int pos=0;
		List<String> lKeyList=new ArrayList<>(lTempMap.keySet());
		for(int i=start; i<lKeyList.size();i++)
		{
			
			Integer count=lTempMap.get(lKeyList.get(i));
			if(count==0)
			{
				continue;
			}
			/*System.out.println("*************");
			System.out.println(lTempMap);
			System.out.println("LEVEL ="+level);
			System.out.println("START="+i);*/
			results[level]=lKeyList.get(i);
			printArray(results);
			lTempMap.put(lKeyList.get(i), count-1);
			/*if(i+1!=countMap.keySet().size())
			{*/
				combinations(lTempMap, results, level+1,i);
			/*}*/
			pos++;
			results[level]=null;
			lTempMap.putAll(countMap);
		}
			
	}

	private static void printArray(String[] results) {
		// TODO Auto-generated method stub
		for(int i=0;i<results.length&&results[i]!=null;i++)
		{
			System.out.print(results[i]+"\t");
		}
		System.out.println("");
	}


}
