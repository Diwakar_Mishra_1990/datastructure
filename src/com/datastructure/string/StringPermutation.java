package com.datastructure.string;

import java.util.HashMap;
import java.util.Map;

public class StringPermutation {
	public static void main(String[] args) {
		String s = "AABC";
		String[] results = new String[s.length()];
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (int i = 0; i < s.length(); i++) {
			if (!countMap.containsKey(s.charAt(i) + "")) {
				countMap.put(s.charAt(i) + "", 1);
			} else {
				countMap.put(s.charAt(i) + "", countMap.get(s.charAt(i) + "") + 1);
			}
		}
		System.out.println(countMap);
		permutations(countMap, results,0);
	}

	private static void permutations(Map<String, Integer> countMap, String[] results,int level) {
		// TODO Auto-generated method stub
		Map<String, Integer> lTempMap = new HashMap<String, Integer>();
		lTempMap.putAll(countMap);
		//System.out.println(lTempMap);
		int totalCount=0;
		for(String s:lTempMap.keySet())
		{
			Integer count=lTempMap.get(s);
			if(count==0)
			{
				continue;
			}
			results[level]=s;
			lTempMap.put(s, count-1);
			permutations(lTempMap, results, level+1);
			lTempMap.putAll(countMap);
			totalCount=totalCount+count;
		}
		if(totalCount==0)
		{
			printArray(results);
		}
	}

	private static void printArray(String[] results) {
		// TODO Auto-generated method stub
		for(int i=0;i<results.length;i++)
		{
			System.out.print(results[i]+"\t");
		}
		System.out.println("");
	}

}
