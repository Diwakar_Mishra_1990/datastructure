package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

public class FindzeroestobeflippedsothatNumberOfConsecutivesONESIsMaximized {

	public static void main(String[] args) {
		Integer []inputs = new Integer[]{1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1};
		int flips = 2;
		findTheFlips(inputs, flips);
	}

	private static void findTheFlips(Integer[] inputs, int flips) {
		// TODO Auto-generated method stub
		Integer[] inputs1 =new Integer[inputs.length];
		inputs1[0]=inputs[0];
		List<Integer> llist = new ArrayList<Integer>();
		List<Integer> lPos = new ArrayList<Integer>();
		if(inputs[0] ==0) {
			lPos.add(0);
		}
		
		for(int i=1;i<inputs.length;i++)  {
			inputs1[i]= inputs1[i-1]+inputs[i];
			if(inputs[i] ==0){
				inputs1[i]=0;
				lPos.add(i);
				if(inputs1[i-1]!=0)
				{
					llist.add(inputs1[i-1]);
				}
			}
		}
		if(inputs1[inputs1.length-1]!=0){
			llist.add(inputs1[inputs1.length-1]);
		}
		printArray(inputs);
		printArray(inputs1);
		findSum(llist,flips,lPos);
		System.out.println(llist);
		System.out.println(lPos);
	}
	private static void findSum(List<Integer> llist, int flips, List<Integer> lPos) {
		// TODO Auto-generated method stub
		if(flips>=llist.size())  {
			flips  = llist.size();
		}
		int pos =flips;
		int sum =0;
		for(int i=0;i<=flips;i++){
			sum+=llist.get(i);
		}
		for (int i = flips+1; i < llist.size(); i++) {
			int tempSum = sum;
			sum = sum + llist.get(i) - llist.get(i - flips+1);
			if (sum > tempSum) {
				pos = i;
			}
		}
		System.out.println(pos);
		int tempPos = pos;
		int tempFlips = flips;
		System.out.println("ZEROS ARE INSERTED AT POSITION");
		sum =  0;
		while (tempFlips>0) {
			System.out.println(lPos.get(tempPos));
		sum+=	llist.get(tempPos);
		tempFlips--;
		tempPos--;
		}
		sum+=	llist.get(tempPos);
		System.out.println("MAXIMUM CONSECUTIVE 1'S ARE " + (sum+flips));
	}

	private static void printArray(Integer[] input) {
		// TODO Auto-generated method stub
		for(int i= 0;i<input.length;i++) {
			System.out.print(input[i]+ "\t");
		}
		System.out.println();
	}
}
