package com.datastructure.Array;

import com.sun.corba.se.spi.orbutil.fsm.Input;

public class ReorderAnArrayAccordingToGivenIndexes {

	public static void main(String[] args) {
		Integer array[] = new Integer[]{50, 40, 70, 60, 90};
		Integer index[] = new Integer[]{3,  0,  4,  1,  2};
	    reArrangeArrays(array, index);
	}

	private static void reArrangeArrays(Integer[] array, Integer[] index) {
		// TODO Auto-generated method stub
		int count=0;
		for(int i=0;i<array.length;i++) {
			if(index[i]==i){
				continue;
			}
			while(index[i]!=i){
				int j = index[i];
				int temp = array[i];
			   array[i]	= array[j];
			   array[j] = temp;
			   
			   temp = index[i];
			   index[i]	= index[j];
			   index[j] = temp;
			   count++;
			}
		}
		printArray(index);
		printArray(array);
		System.out.println(count);
	}
	private static void printArray(Integer[] input) {
		// TODO Auto-generated method stub
		for(int i= 0;i<input.length;i++) {
			System.out.print(input[i]+ "\t");
		}
		System.out.println();
	}
}
