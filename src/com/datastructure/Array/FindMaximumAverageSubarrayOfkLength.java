package com.datastructure.Array;

public class FindMaximumAverageSubarrayOfkLength {

	public static void main(String[] args) {
		Integer input[] = { 1, 12, -5, -6, 50, 3 };
		int windowSize = 4;
		getMaximumAverageSubarrayOfkLength(input, windowSize);
	}

	private static void getMaximumAverageSubarrayOfkLength(Integer[] input, int windowSize) {
		// TODO Auto-generated method stub
		float average = 0f;
		int sum = 0;
        for(int i=0;i<windowSize;i++) {
        	sum +=input[i];
        }
        average = (float)sum/windowSize;
        int tempSum = sum;
        int pos =windowSize-1;
        for(int i=windowSize;i<input.length;i++){
        	tempSum =tempSum -input[i-windowSize]+
        			input[i];
        	if(tempSum>sum){
        		sum = tempSum;
        		pos = i;
        	}
        }
        printArray(input,pos,windowSize);
	}

	private static void printArray(Integer[] input, int pos, int windowSize) {
		// TODO Auto-generated method stub
		for(int i= pos-windowSize+1; i<=pos;i++){
			System.out.print(input[i]+"\t");
		}
		System.out.println();
	}
}
