package com.datastructure.Array;

/**
 * 
 * @author Diwakar Mishra
 *We are given two sorted array. We need to merge these two arrays such that the initial numbers (after complete sorting) are in the first array and the remaining numbers are in the second array. Extra space allowed in O(1).

Example:

Input: ar1[] = {10};
       ar2[] = {2, 3};
Output: ar1[] = {2}
        ar2[] = {3, 10}  

Input: ar1[] = {1, 5, 9, 10, 15, 20};
       ar2[] = {2, 3, 8, 13};
Output: ar1[] = {1, 2, 3, 5, 8, 9}
        ar2[] = {10, 13, 15, 20}
 */
public class MergeTwoSortedArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer []input1 = new Integer[]{1, 5, 9, 10, 15, 20};
		Integer []input2 = new Integer[]{2, 3, 8, 13};
		mergeInputArrays(input1, input2);
		printArray(input1);
		printArray(input2);
	}

	private static void printArray(Integer[] input1) {
		// TODO Auto-generated method stub
		for(int i=0;i<input1.length;i++) {
			System.out.print(input1[i]+ "\t");
		}
		System.out.println();
	}

	private static void mergeInputArrays(Integer[] input1, Integer[] input2) {
		// TODO Auto-generated method stub
		int length1 = input1.length-1;
		int totalLength = length1 + input2.length;
		int i=0;
		while(i<=totalLength) {
			int j = i;
			if(i<=length1) {
				while(j<=totalLength) {
					if(j<=length1) {
						if(input1[i]>input1[j]) {
							int temp = input1[i];
							input1[i]= input1[j];
							input1[j]=temp;
							
						}
					}
					else {
	                    if(input1[i]>input2[j-length1-1]) {
	                    	int temp = input1[i];
							input1[i]= input2[j-length1-1];
							input2[j-length1-1]=temp;
						}
					}
					j++;
				}
			} else {
				while(j<=totalLength) {
	                    if(input2[i-length1-1]>input2[j-length1-1]) {
	                    	int temp = input2[i-length1-1];
							input2[i-length1-1]= input2[j-length1-1];
							input2[j-length1-1]=temp;
						}
	                    j++;
				}
			}
			i++;
		}
	}

}
