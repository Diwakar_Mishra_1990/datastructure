package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.bcel.internal.generic.DUP;
import com.sun.xml.internal.fastinfoset.util.DuplicateAttributeVerifier;

/**
 * @author Diwakar Mishra
 *Given an array of n elements which contains elements from 0 to n-1, 
 *with any of these numbers appearing any number of times. Find these 
 *repeating numbers in O(n) and using only constant memory space.
 *For example, let n be 7 and array be {1, 2, 3, 1, 3, 6, 6}, the answer should be 1, 3 and 6
 */
public class FindDuplicates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer[] input = new Integer[]{1, 2, 3, 1, 3, 6, 6};
		List<Integer> duplicateList=findDuplicates(input);
		System.out.println(duplicateList);
	}

	private static List<Integer> findDuplicates(Integer[] input) {
		// TODO Auto-generated method stub
		List<Integer> dulicateList = new ArrayList<Integer>();
		for(int i=0;i<input.length;i++)
		{
			if(input[Math.abs(input[i])]>=0)
			{
				input[Math.abs(input[i])]=-input[Math.abs(input[i])];
			}
			else
			{
				dulicateList.add(Math.abs(input[i]));
			}
		}
		return dulicateList;
	}

}
