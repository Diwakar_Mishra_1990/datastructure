package com.datastructure.Array;

/**
 * @author Diwakar Mishra Write an efficient C program to find the sum of
 *         contiguous subarray within a one-dimensional array of numbers which
 *         has the largest sum.
 */
public class LargestSumContiguousSubarray {
	public static void main(String[] args) {
		Integer []inputs =new Integer[]{-2, -3, 4, -1, -2, 1, 5, -3};
		Integer  []result=getMaxIntegerByKadanesAlgorithm(inputs);
	}

	private static Integer[] getMaxIntegerByKadanesAlgorithm(Integer[] inputs) {
		// TODO Auto-generated method stub
		Integer  []result= new Integer[inputs.length];
		int maxInclusive = 0;
		int maxExclusive =0;
		int start =0, end = 0, s=0;
		for(int i=0;i<inputs.length;i++)
		{
			maxInclusive= maxInclusive +inputs[i];	
			if(maxInclusive<0)
			{
				maxInclusive=0;
				s =i+1;
			}
			if(maxInclusive>maxExclusive)
			{
				start  =s;
				end= i;
				maxExclusive=maxInclusive;
			}
		}
		
		System.out.println(maxExclusive);
		System.out.println(start);
		System.out.println(end);
		return null;
	}
}
