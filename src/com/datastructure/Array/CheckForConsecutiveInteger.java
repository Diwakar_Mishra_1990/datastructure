package com.datastructure.Array;

public class CheckForConsecutiveInteger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer[] inputs = new Integer[]{5, 2, 3, 1, 4};
		getConsecutiveNumber(inputs);
	}

	private static void getConsecutiveNumber(Integer[] inputs) {
		// TODO Auto-generated method stub
		int max = getMaxElement(inputs);
		int min = getMinElement(inputs);
		boolean[] visited = new boolean[inputs.length];
		if(max-min+1 != inputs.length) {
			System.out.println("CONECUTIVE ELEMENTS NOT AVAILABLE");
			return;
		}
		for(int i=0;i<inputs.length;i++)
		{
			if(!visited[inputs[i]-min])
			{
				visited[inputs[i]-min] = true;
			}
			else
			{
				System.out.println("CONECUTIVE ELEMENTS NOT AVAILABLE");
				return;
			}
			
		}
		System.out.println("CONSECUTIVE ELEMENT");
		System.out.println("STARTING FROM : " +min);
		System.out.println("ENDING AT : " +max);
		
		
	}

	private static int getMinElement(Integer[] inputs) {
		// TODO Auto-generated method stub
		int min = Integer.MAX_VALUE;
		for(int i=0 ;i<inputs.length;i++)
		{
			min = Math.min(min,inputs[i]);
		}
		return min;
	}

	private static int getMaxElement(Integer[] inputs) {
		// TODO Auto-generated method stub
		int max = Integer.MIN_VALUE;
		for(int i=0 ;i<inputs.length;i++)
		{
			max = Math.max(max,inputs[i]);
		}
		return max;
	}

}
