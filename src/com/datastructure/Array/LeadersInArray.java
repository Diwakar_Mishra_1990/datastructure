package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Diwakar Mishra
 *Write a program to print all the LEADERS in the array. 
 *An element is leader if it is greater than all the elements 
 *to its right side. And the rightmost element is always a leader. 
 *For example int the array {16, 17, 4, 3, 5, 2}, leaders are 17, 5 and 2.
 */
public class LeadersInArray {
	public static void main(String[] args) {
		Integer[] inputs = new Integer[] { 16, 17, 4, 3, 5, 2 };
		List<Integer> leaderList = new ArrayList<Integer>();
		getLeader(inputs, leaderList);
		System.out.println(leaderList);
	}

	private static void getLeader(Integer[] inputs, List<Integer> leaderList) {
		// TODO Auto-generated method stub
		int max=Integer.MIN_VALUE;
		for(int i=inputs.length-1;i>=0;i--)
		{
		if(inputs[i]>max)
		{
			max=inputs[i];
			leaderList.add(max);
		}
		}
	}
}
