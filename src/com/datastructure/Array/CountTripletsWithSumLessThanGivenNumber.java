package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

public class CountTripletsWithSumLessThanGivenNumber {

	public static void main(String[] args) {
		Integer[] inputs  = new Integer[] {-2, 0, 1, 3};
		int sum = 2;
		getTripletsWithSumLessThanGivenNumber(inputs, sum);
	}

	private static void getTripletsWithSumLessThanGivenNumber(Integer[] inputs, int sum) {
		// TODO Auto-generated method stub
		sortInputs(inputs);
		int sumtemp =0;
	    List<Integer> pos = new ArrayList<Integer>();
		for(int i=0;i<=2;i++){
			sumtemp+=inputs[i];
		}
		if(sumtemp>sum)  {
			System.out.println("NO TRIPLETS EXIST");
			return;
		}
		else
		{
			pos.add(2);
		}
		for(int i=3;i<inputs.length;i++) {
			sumtemp= sumtemp+inputs[i]-inputs[i-3];
			if(sumtemp<sum) {
				pos.add(i);
			}
		}
		for(int i=0;i<pos.size();i++)  {
			printArray(inputs,pos.get(i));
		}
	}
	private static void printArray(Integer[] input, int pos) {
		// TODO Auto-generated method stub
		for(int i= pos-2;i<=pos;i++) {
			System.out.print(input[i]+ "\t");
		}
		System.out.println();
	}

	private static void sortInputs(Integer[] inputs) {
		// TODO Auto-generated method stub
		for(int i=0;i<inputs.length;i++) {
			for(int j=i;j<inputs.length;j++) {
				if(inputs[i]>inputs[j]) {
					int temp = inputs[j];
					inputs[j] = inputs[i];
					inputs[i] = temp;
				}
			}
		}
		printArray(inputs);
	}

	private static void printArray(Integer[] inputs) {
		// TODO Auto-generated method stub
		for(int i=0;i<inputs.length;i++) {
			System.out.print(inputs[i] + "\t");
		}
		System.out.println();
	}
}
