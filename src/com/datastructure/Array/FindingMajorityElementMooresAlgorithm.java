package com.datastructure.Array;

/**
 * @author Diwakar Mishra
 *Majority Element: A majority element in an array 
 *A[] of size n is an element that appears more than n/2 
 *times (and hence there is at most one such element).
 *Write a function which takes an array and emits the majority 
 *element (if it exists), otherwise prints NONE as follows:
 *
 *Space Complexity =O(1)
 *Time Complexiy =O(n)
 */
public class FindingMajorityElementMooresAlgorithm {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer []inputs = new Integer[]{2,2,3,5,2,2,6};
	    int lmajorityElement = getMajorityElement(inputs); 
	    System.out.println(lmajorityElement);
	}

	private static int getMajorityElement(Integer[] inputs) {
		// TODO Auto-generated method stub
		int majorityIndex=0;
		int count=0;
		for(int i=0;i<inputs.length;i++)
		{
			if(count==0)
			{
				majorityIndex=i;
				count++;
			}
			else if(inputs[i]==inputs[majorityIndex])
			{
				count++;
			}
			else
			{
				count--;
			}
			
		}
		return inputs[majorityIndex];
	}
}
