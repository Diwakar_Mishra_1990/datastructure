package com.datastructure.Array;

public class TurnImageBy90Deg {

	public static void main(String[] args) {
		String[][] inputs=new String[][]{
			{"*","*","*","^","*","*","*"},
			{"*","*","*","|","*","*","*"},
			{"*","*","*","|","*","*","*"},
			{"*","*","*","|","*","*","*"}};
		String[][] outputs=new String[inputs[0].length][inputs.length];
	    rotateImageBy90Deg(inputs,outputs);
	    System.out.println("INPUTS::::::::::::");
	    printArray(inputs);
	    System.out.println("OUTPUTS::::::::::::");
	    printArray(outputs);
	     
	}
	
	private static void rotateImageBy90Deg(String[][] inputs, String[][] outputs) {
		// TODO Auto-generated method stub
		String[]temPArray=null;
		for(int i=0;i<inputs.length;i++)
		{
			temPArray=inputs[i];
			for(int j=0;j<temPArray.length;j++)
			{
				if(temPArray[j].equals("^"))
				{
					outputs[j][i]="<";
				}
				else if(temPArray[j].equals("|"))
				{
					outputs[j][i]="-";
				}
				else
				{
					outputs[j][i]=temPArray[j];
				}
			}
		}
	}

	private static void printArray(String[][] outputs) {
		// TODO Auto-generated method stub
		for (int i = 0; i < outputs.length; i++) {
			for (int j = 0; j < outputs[i].length; j++) {
				System.out.print(outputs[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
