package com.datastructure.Array;

/**
 * @author Diwakar Mishra Given an unsorted array arr[] and two numbers x and y,
 *         find the minimum distance between x and y in arr[]. The array might
 *         also contain duplicates. You may assume that both x and y are
 *         different and present in arr[].
 * 
 *         Examples: Input: arr[] = {1, 2}, x = 1, y = 2 Output: Minimum
 *         distance between 1 and 2 is 1.
 * 
 *         Input: arr[] = {3, 4, 5}, x = 3, y = 5 Output: Minimum distance
 *         between 3 and 5 is 2.
 * 
 *         Input: arr[] = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3}, x = 3, y = 6
 *         Output: Minimum distance between 3 and 6 is 4.
 * 
 *         Input: arr[] = {2, 5, 3, 5, 4, 4, 2, 3}, x = 3, y = 2 Output: Minimum
 *         distance between 3 and 2 is 1.
 */
public class MinimumDistanceBetweenTwoNumbers {

	public static void main(String[] args) {

		Integer[] inputs = new Integer[] {2, 5, 3, 5, 4, 4, 2, 3};
		int firstNumber = 3;
		int secondNumber = 2;
		System.out.println(getMinimumDistanceBetweenTwoNumbers(inputs, firstNumber, secondNumber));
	}

	private static int getMinimumDistanceBetweenTwoNumbers(Integer[] inputs, int firstNumber, int secondNumber) {
		// TODO Auto-generated method stub
		int start = 0;
		int diff = Integer.MAX_VALUE;
		for (int i = 0; i < inputs.length; i++) {
			if (inputs[i] == firstNumber || (inputs[i] == secondNumber)) {
				start = i;
				break;
			}
		}
		for (int i = start; i < inputs.length; i++) {
			if (inputs[i] == firstNumber || inputs[i] == secondNumber) {
				if (inputs[start] == inputs[i]) {
					start = i;
				}
				else
				{
					diff = Math.min(diff, Math.abs(i-start));
				}
			}
				

		}
		return diff;
	}

}
