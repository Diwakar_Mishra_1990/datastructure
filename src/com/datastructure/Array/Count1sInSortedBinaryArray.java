package com.datastructure.Array;

public class Count1sInSortedBinaryArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer arr[] = new Integer[]{1, 1, 0, 0, 0, 0, 0};
		find1sInSortedBinaryArray(arr);
	}

	private static void find1sInSortedBinaryArray(Integer[] arr) {
		// TODO Auto-generated method stub
		int end = arr.length-1;
		int mid =  findLastPositionOf1(arr,0,end);
		System.out.println(mid);
	}

	private static int findLastPositionOf1(Integer[] arr, int start, int end) {
		// TODO Auto-generated method stub
		int mid = (start+end)/2;
		if(arr[mid]==1 && arr[mid+1]==0) {
			return mid;
		}
		else if(arr[mid]==1 && arr[mid+1]==1){
			return findLastPositionOf1(arr, mid,end);
		}
		else {
			return findLastPositionOf1(arr, start,mid);
		}
	}

}
