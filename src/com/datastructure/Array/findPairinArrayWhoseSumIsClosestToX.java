package com.datastructure.Array;

public class findPairinArrayWhoseSumIsClosestToX {
public static void main(String[] args) {
	Integer[] input = new Integer[] {10, 22, 28, 29, 30, 40};
	int right =0;
	int left =input.length-1;
	int x  = 54;
	getClosestSumToX(input,x,right,left);
}

private static void getClosestSumToX(Integer[] input, int x, int right, int left) {
	// TODO Auto-generated method stub
	int diff = Integer.MAX_VALUE;
	int rightMin = 0;
	int leftMin = 0;
	while(right<left) {
		if(Math.abs(input[right]+input[left]-x)<diff){
			diff = Math.abs(input[right]+input[left]-x);
			rightMin  = right;
			leftMin  = left;
		}
		if(input[right]+input[left]>x)  {
			left--;
		}
		else {
			right++;
		}
	}
	System.out.println("Minimum right = "+input[rightMin]);
	System.out.println("Minimum left = "+input[leftMin]);
}
}
