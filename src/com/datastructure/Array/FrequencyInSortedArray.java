package com.datastructure.Array;

public class FrequencyInSortedArray {

	public static void main(String[] args) {
		 Integer input[] = new Integer[]{1, 1, 2, 2, 2, 2, 3,4};
		 int searchElement  = 3;
		 getFrequencyInAerray(input, searchElement);
	}

	private static void getFrequencyInAerray(Integer[] input, int searchElement) {
		// TODO Auto-generated method stub
		int start =0;
		int end = input.length-1;
		int first = getFirstOccurenceOfSearxhElement(input, searchElement ,start ,end);
		int last = getLastOccurenceOfSearxhElement(input, searchElement ,start ,end);
		System.out.println("Element is :"+ searchElement);
		System.out.println("First Element:" +first);
		System.out.println("Last Element:"+last);
	}

	private static int getLastOccurenceOfSearxhElement(Integer[] input, int searchElement, int start, int end) {
		// TODO Auto-generated method stub
		int mid = (start+end)/2;
		if((mid ==input.length-1 || searchElement<input[mid+1]) && input [mid] == searchElement){
			return mid;
		}
		else if(searchElement<input[mid])
		{
			return getLastOccurenceOfSearxhElement(input,searchElement,
			                                              start,mid-1);
		}
		else
		{
			return getLastOccurenceOfSearxhElement(input,searchElement,
                    mid+1,end);
		}
	}

	private static int getFirstOccurenceOfSearxhElement(Integer[] input, int searchElement,
			                                              int start, int end) {
		// TODO Auto-generated method stub
		int mid = (start+end)/2;
		if((mid ==0 || searchElement>input[mid-1]) && input [mid] == searchElement){
			return mid;
		}
		else if(searchElement>input[mid])
		{
			return getFirstOccurenceOfSearxhElement(input,searchElement,
			                                              mid+1,end);
		}
		else
		{
			return getFirstOccurenceOfSearxhElement(input,searchElement,
                    start, mid-1);
		}
	}
}
