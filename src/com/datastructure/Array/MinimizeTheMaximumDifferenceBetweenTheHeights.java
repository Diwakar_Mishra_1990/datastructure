package com.datastructure.Array;

/**
 * 
 * @author Diwakar Mishra
 *Given heights of n towers and a value k. We need to either increase or decrease height of every tower by k (only once) where k > 0. The task is to minimize the difference between the heights of the longest and the shortest tower after modifications, and output this difference.

Examples:

Input  : arr[] = {1, 15, 10}, k = 6
Output : arr[] = {7, 9, 4}
         Maximum difference is 5.
Explanation : We change 1 to 6, 15 to 
9 and 10 to 4. Maximum difference is 5
(between 4 and 9). We can't get a lower
difference.

Input : arr[] = {1, 5, 15, 10} 
        k = 3   
Output : arr[] = {4, 8, 12, 7}
Maximum difference is 8

Input : arr[] = {4, 6} 
        k = 10
Output : arr[] = {14, 16} OR {-6, -4}
Maximum difference is 2

Input : arr[] = {6, 10} 
        k = 3
Output : arr[] = {9, 7} 
Maximum difference is 2

Input : arr[] = {1, 10, 14, 14, 14, 15}
        k = 6 
Output: arr[] = {7, 4, 8, 8, 8, 9} 
Maximum difference is 5

Input : arr[] = {1, 2, 3}
        k = 2 
Output: arr[] = {3, 4, 5} 
Maximum difference is 2
 */
public class MinimizeTheMaximumDifferenceBetweenTheHeights {

	public static void main(String[] args) {
		Integer[] inputs = new Integer[]{1, 10, 14, 14, 14, 15};
		int differenceMaker = 6;
		rearrangeLength(inputs, differenceMaker);
		printArray(inputs);
	}

	private static void printArray(Integer[] inputs) {
		// TODO Auto-generated method stub
		for(int i=0; i<inputs.length;i++){
			System.out.print(inputs[i]+"\t");
		}
		System.out.println("");
	}

	private static void rearrangeLength(Integer[] inputs, int differenceMaker) {
		// TODO Auto-generated method stub
		int mean = 0;
		for(int i=0;i<inputs.length;i++) {
			mean+=inputs[i];
		}
		System.out.println((float)(mean)/(inputs.length-1));
		mean = mean/inputs.length;
		for(int i=0;i<inputs.length;i++) {
			if(mean<differenceMaker){
				inputs[i]+=differenceMaker;
			}
			else {
				if(inputs[i]<mean){
					inputs[i]+=differenceMaker;
				}
				else {
					inputs[i]-=differenceMaker;
				}
			}
		}
		
	}
}
