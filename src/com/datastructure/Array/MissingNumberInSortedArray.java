package com.datastructure.Array;

/**
 * @author Diwakar Mishra
 *Given a sorted array of n distinct integers where each integer is in the range from 0 to m-1 and m > n. Find the smallest number that is missing from the array.

Examples
Input: {0, 1, 2, 6, 9}, n = 5, m = 10
Output: 3

Input: {4, 5, 10, 11}, n = 4, m = 12
Output: 0

Input: {0, 1, 2, 3}, n = 4, m = 5
Output: 4

Input: {0, 1, 2, 3, 4, 5, 6, 7, 10}, n = 9, m = 11
Output: 8
 */
public class MissingNumberInSortedArray {

	public static void main(String[] args) {
		Integer[] input = new Integer[]{0,1,2,6,9};
		int m =10;
		findLowestMssingNumber(input,m);
	}

	private static void findLowestMssingNumber(Integer[] input, int m) {
		// TODO Auto-generated method stub
	 int start =0;
	 int end = input.length-1;
	 int missing = getPosOfLoweestMissingNumber(input, start, end);
	 System.out.println("MISSING: "+missing);
	}

	/**
	 * @param input
	 * @param start
	 * @param end
	 */
	private static int getPosOfLoweestMissingNumber(Integer[] input, int start, int end) {
		int mid= (start+end)/2;
		if(start==end ){
			if(mid== input[mid])
			{
			System.out.println("ALL IN ORDER");
			return -1;
			}
			else
			{
				return mid;
			}
		}
		
		 if(input[mid] <mid)
		 {
			 return getPosOfLoweestMissingNumber(input, start, mid);
		 }
		 else
		 {
			 return getPosOfLoweestMissingNumber(input, mid+1, end);
		 }
	}
	
}
