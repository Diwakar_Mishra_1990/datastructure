package com.datastructure.Array;

public class LeastAverageSubArrayWithGivenWindowSize {

	public static void main(String[] args) {
		Integer[] input = new Integer[] { 3, 7, 90, 20, 10, 50, 40 };
		int windowSize = 3;
		getMinAvg(input, windowSize);
	}

	private static void getMinAvg(Integer[] input, int windowSize) {
		// TODO Auto-generated method stub
		float average = 0f;
		float minAverage = Float.MAX_VALUE;
		int sum = 0;
		for (int i = 0; i < windowSize; i++) {
			sum+=input[i];
		}
		average = ((float) sum) / windowSize;
		int pos = windowSize - 1;
		minAverage = average;
		for (int i = 3; i < input.length; i++) {
			sum = sum + input[i] - input[i - 3];
			average = ((float) sum) / windowSize;
			if (average < minAverage) {
				minAverage = average;
				pos = i;
			}
		}
		System.out.println("THE MINIMUM AVERAGE IS : "+minAverage );
		printArray(input , pos);
	}

	private static void printArray(Integer[] input, int pos) {
		// TODO Auto-generated method stub
		for(int i= pos-2;i<=pos;i++) {
			System.out.print(input[i]+ "\t");
		}
		System.out.println();
	}
}
