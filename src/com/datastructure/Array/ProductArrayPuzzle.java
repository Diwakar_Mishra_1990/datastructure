package com.datastructure.Array;


/**
 * @author Diwakar Mishra
 * Given an array arr[] of n integers,
 * construct a Product Array prod[] (of same size)
 * such that prod[i] is equal to the product of all the 
 * elements of arr[] except arr[i]. 
 * Solve it without division operator and in O(n).
 */
public class ProductArrayPuzzle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer[] inputs = new Integer[] { 10, 3, 5, 6, 2 };
		Integer[] productArray= new Integer[] { 1, 1, 1, 1, 1 };
		getProduct(inputs,productArray);
		printArray(productArray);
	}

	private static void printArray(Integer[] productArray) {
		// TODO Auto-generated method stub
		for(int i=0;i<productArray.length;i++)
		{
			System.out.print(productArray[i]+"\t");
		}
		System.out.println();
	}

	private static void getProduct(Integer[] inputs, Integer[] productArray) {
		// TODO Auto-generated method stub
		int leftMul=1;
		int rightMul=1;
		for(int i=0;i<inputs.length;i++)
		{
			if(i!=0)
			{
				rightMul= rightMul*inputs[i-1];
				productArray[i]=productArray[i]*rightMul;
			}
			int j= inputs.length-1-i;
			if(j!=inputs.length-1)
			{
				leftMul=leftMul*inputs[j+1];
				productArray[j]=productArray[j]*leftMul;
			}
			
		}
		
	}

}
