package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Diwakar Mishra
 *Question: An Array of sorted integers is given, both +ve and -ve. 
 *You need to find the two elements such that their sum is closest
 * to zero.  For the below array, program should print -80 and 85.
 */
public class TwoElementsWhoseSumisClosestToZero {

	public static void main(String[] args) {
		Integer []inputs= new Integer[]{-80,-10,1,60,70,85};
		List<Integer> closectList=new ArrayList<>();
		getClosestIntegerToZero(inputs,closectList);
		System.out.println("The Minimumsum is " +closectList.get(2));
		System.out.println("The Minimum sum contributor are " +inputs[closectList.get(0)] +" and " + inputs[closectList.get(1)]);
	}

	private static void getClosestIntegerToZero(Integer[] inputs, List<Integer> closectList) {
		// TODO Auto-generated method stub
		int left=0;
		int right =inputs.length-1;
		int minLeft=0;
		int minRight=0;
		int minSum=Integer.MAX_VALUE;
		while(left<right)
		{
			int sum =inputs[left]+inputs[right];
			if(Math.abs(sum)<Math.abs(minSum))
			{
				minSum=Math.abs(sum);
				minLeft=left;
				minRight=right;
			}
			if(sum<0)
			{
			left++;
			}
			else if(sum>0)
			{
				right--;
			}
			else
			{
				minSum=sum;
				minLeft=left;
				minRight=right;
				break;
			}
		}
		closectList.add(minLeft);
		closectList.add(minRight);
		closectList.add(minSum);
		
	}
}
