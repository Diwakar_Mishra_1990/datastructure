package com.datastructure.Array;

import com.sun.javafx.collections.SetAdapterChange;

/**
 * @author Diwakar Mishra
 *Given a sorted array and a value x, 
 *the ceiling of x is the smallest element 
 *in array greater than or equal to x, and the 
 *floor is the greatest element smaller than or equal to x. 
 *Assume than the array is sorted in non-decreasing order. 
 *Write efficient functions to find floor and ceiling of x.
 */
public class FloorInASortedArray {

	public static void main(String[] args) {
		Integer[] input=new Integer[]{1, 2, 8, 10, 10, 12, 19};
		int inputNumber = 0;
		int ceiling=getFloor(input,inputNumber ,0 ,input.length-1);
		System.out.println(ceiling);
	}

	private static Integer getFloor(Integer[] input, int inputNumber, int start, int end) {
		// TODO Auto-generated method stub
		if(start == end)
		{
			if(input[start]<=inputNumber)
			{
				return input[start];
			}
			return -1;
		}
		else
		{
			int mid= (int) Math.ceil(((float)(start+end))/2);
			if(input[mid]>=inputNumber)
			{
				return getFloor(input, inputNumber, start, mid-1);
			}
			else
			{
				return getFloor(input, inputNumber, mid, end);
			}
		}
	}
}
