package com.datastructure.Array;

/**
 * 
 * @author Diwakar Mishra
 *Given an array of integers, count number of subarrays (of size more than one) that are strictly increasing.
Expected Time Complexity : O(n)
Expected Extra Space: O(1)

Examples:

Input: arr[] = {1, 4, 3}
Output: 1
There is only one subarray {1, 4}

Input: arr[] = {1, 2, 3, 4}
Output: 6
There are 6 subarrays {1, 2}, {1, 2, 3}, {1, 2, 3, 4}
                      {2, 3}, {2, 3, 4} and {3, 4}

Input: arr[] = {1, 2, 2, 4}
Output: 2
There are 2 subarrays {1, 2} and {2, 4}
 */
public class StrictlyIncreasingSubArray {
 
	public static void main(String[] args) {
		Integer []inputs = new Integer[]{1, 4, 3};
		getLargestIncreasingSubarray(inputs);
	}

	private static void getLargestIncreasingSubarray(Integer[] inputs) {
		// TODO Auto-generated method stub
		Integer inputSubArray[] = new Integer[inputs.length];
		inputSubArray[0]=1;
		for(int i=1;i<inputs.length;i++) {
			if(inputs[i]>inputs[i-1]) {
				inputSubArray[i]=inputSubArray[i-1]+ 1;
			}
			else {
				inputSubArray[i]=1;
			}
		}
		int max =0;
		int pos = 0;
		int start = 0; 
		for(int i=0;i<inputSubArray.length;i++){
			if(inputSubArray[i]>max){
				pos = i;
				max = inputSubArray[i];
			}
		}
		int end = pos;
		while(inputSubArray[pos]!=1) {
			pos--;
		}
		start = pos;
		for(int i=start;i<=end;i++) {
			System.out.print(inputs[i]+"\t");
		}
		System.out.println();
	}
}
