package com.datastructure.Array;


/**
 * @author Diwakar Mishra
 *Given an array of positive integers. All numbers occur even 
 *number of times except one number which occurs odd number of times. 
 *Find the number in O(n) time & constant space.
 *Example:
 *I/P = [1, 2, 3, 2, 3, 1, 3]
 *O/P = 3
 */
public class NumberOccurringOddNumberofTimes {
	public static void main(String[] args) {
		Integer []input= new Integer[]{1, 2, 3, 2, 3, 1, 3};
		int oddNumber=getUniqueOddoccurenceNumber(input);
		System.out.println(oddNumber);
	}

	private static int getUniqueOddoccurenceNumber(Integer[] input) {
		// TODO Auto-generated method stub
		int res=0;
		for(int i=0;i<input.length;i++)
		{
			res=res^input[i];
		}
		return res;
	}
}
