package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Diwakar Mishra
 * Write a C function to return minimum and maximum in an array. 
 * You program should make minimum number of comparisons.
 */
public class GetMinMaxInMinimumComparison {

	public static void main(String[] args) {
		Integer []inputs= new Integer[]{1000, 11, 445, 1,3453253,324,24314224,24422, 330, 3000};
		Integer[] minMaxArray=new Integer[]{Integer.MAX_VALUE,Integer.MIN_VALUE};
		getminMax(inputs,minMaxArray,0,inputs.length-1);
		System.out.println(minMaxArray[0]+" and "+ minMaxArray[1]);
	}

	private static void getminMax(Integer[] inputs, Integer[] minMaxArray,int start,int end) {
		// TODO Auto-generated method stub
		if(start<=end)
		{
			if(end-start==1)
			{
				if(inputs[end]>inputs[start])
				{
					minMaxArray[0]=Math.min(inputs[start], minMaxArray[0]);
					minMaxArray[1]=Math.max(inputs[end], minMaxArray[1]);
				}
				else
				{
					minMaxArray[0]=Math.min(inputs[end], minMaxArray[0]);
					minMaxArray[1]=Math.max(inputs[start], minMaxArray[1]);
				}
			}
			else if(end-start==0)
			{
				minMaxArray[0]=Math.min(inputs[end], minMaxArray[0]);
				minMaxArray[1]=Math.max(inputs[start], minMaxArray[1]);
			}
			else {
				int mid= (start+end)/2;
				getminMax(inputs, minMaxArray, start, mid);
				getminMax(inputs, minMaxArray, mid+1, end);
			}
			}
		}
		
	}

