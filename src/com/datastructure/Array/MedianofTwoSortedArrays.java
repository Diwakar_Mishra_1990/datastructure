package com.datastructure.Array;

import java.awt.print.Printable;

/**
 * @author Diwakar Mishra
 *
 */
public class MedianofTwoSortedArrays {
	public static void main(String[] args) {
		Integer[] input1= new Integer[]{1, 12, 15, 26, 38};
		Integer[] input2= new Integer[]{2, 13, 17, 30, 45};
		int median=findMedian(input1,0,input1.length-1,input2,0,input2.length-1);
		System.out.println(median);
	}

	private static int findMedian(Integer[] input1, int low1,
			int high1, Integer[] input2, int low2, int high2) {
		// TODO Auto-generated method stub
		printArray(input1 ,low1,high1);
		printArray(input2 ,low2,high2);
		if((high1-low1)==1 && (high2-low2)==1)
		{
			return(Math.max(input1[low1], input2[low2])+Math.min(input1[high1], input2[high2]))/2;
		}
		//float mid1= (float)high1/2;
		int mid1= input1[(low1+high1)/2];

		/*if(mid1>high1/2)
		{
			mid1=high1/2+1;
		}*/
		//float mid2= (float)high2/2;
		int mid2= input2[(low2+high2)/2];
		/*if(mid1>high2/2)
		{
			mid2=high2/2+1;
		}*/
		if(mid1<mid2)
		{
			return findMedian(input1, (low1+high1)/2,
					 high1, input2,  low2, (low2+high2)/2);
		}
		else if(mid2<mid1)
		{
			return findMedian(input1,low1 ,
					(low1+high1)/2, input2,  (low2+high2)/2,high2 );
		}
		else
		{
			System.out.println(low1+","+high1+","+low2+","+high2);
			return high1/2;//or high2/2
		}
	}

	private static void printArray(Integer[] input1, int low1, int high1) {
		// TODO Auto-generated method stub
		for(int i=low1;i<=high1;i++)
		{
			System.out.print(input1[i]+"\t");
		}
		System.out.println();
	}
}
