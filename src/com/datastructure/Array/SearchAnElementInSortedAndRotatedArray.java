package com.datastructure.Array;

/**
 * @author Diwakar Mishra
 *An element in a sorted array can be found in O(log n)
 * time via binary search. But suppose we rotate an ascending
 *  order sorted array at some pivot unknown to you beforehand. 
 *  So for instance, 1 2 3 4 5 might become 3 4 5 1 2. 
 *  Devise a way to find an element in the rotated array in O(log n) 
 *  time.
 */
public class SearchAnElementInSortedAndRotatedArray {

	public static void main(String[] args) {
		Integer input[] = new Integer[]{5, 6, 7, 8, 9, 10, 1, 2, 3};
		int searchElement = 2;
		int index= findSearchElement(input,searchElement);
		System.out.println(index);
	}

	private static int findSearchElement(Integer[] input, int searchElement) {
		// TODO Auto-generated method stub
		int pivot =findPivotElement(input,0,input.length-1);
		System.out.println(pivot);
		if(input[pivot]<searchElement &&
				searchElement<input[input.length-1])
		{
			 return serchElementInInput(input,pivot-1,input.length-1,searchElement);
		}
		else 
		{
			return serchElementInInput(input,0,pivot,searchElement);
		}
	}

	private static int serchElementInInput(Integer[] input, int high, int low, int searchElement) {
		// TODO Auto-generated method stub
		int mid= (high+low)/2;
		if(searchElement>input[mid])
		{
			return serchElementInInput(input,mid+1,input.length-1,searchElement);
		}
		else if(searchElement<input[mid])
		{
			return serchElementInInput(input,low,mid,searchElement);
		}
		else
		{
			return mid;
		}
		
	}

	private static int findPivotElement(Integer[] input, int low, int high) {
		// TODO Auto-generated method stub
		int mid = (low+high)/2;
		 if(input[mid]>input[high])
		{
				return findPivotElement(input,mid+1,high);
		}
		 else if(input[mid]<input[low])
		 {
			 return findPivotElement(input,low,mid);
		 }
		 else
		 {
			 return mid;
		 }
	}
}
