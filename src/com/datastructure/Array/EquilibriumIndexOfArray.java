package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Diwakar Mishra
 *Equilibrium index of an array is an index such that
 * the sum of elements at lower indexes is equal to the 
 * sum of elements at higher indexes. For example, in an arrya A:
 * A[0] = -7, A[1] = 1, A[2] = 5, A[3] = 2, A[4] = -4, A[5] = 3, A[6]=0
 * 3 is an equilibrium index, because:
 * A[0] + A[1] + A[2] = A[4] + A[5] + A[6]
 * 6 is also an equilibrium index, because sum of zero elements is zero, 
 * i.e., A[0] + A[1] + A[2] + A[3] + A[4] + A[5]=0
 * 7 is not an equilibrium index, because it is not a valid index of array A.
 * Write a function int equilibrium(int[] arr, int n); 
 * that given a sequence arr[] of size n, returns an equilibrium index (if any) or -1 
 * if no equilibrium indexes exist.
 */
public class EquilibriumIndexOfArray {

	public static void main(String[] args) {
		Integer[] inputs= new Integer[]{-7, 1, 5, 2, -4, 3, 0};
		List<Integer> equilibriumIndex=getEquilibriumIndex(inputs);
		System.out.println(equilibriumIndex);
	}

	/**
	 * @param inputs 
	 * @return
	 */
	private static List<Integer> getEquilibriumIndex(Integer[] inputs) {
		// TODO Auto-generated method stub
		List<Integer> equilibriumIndexList = new ArrayList<Integer>();
		int sum=0;
		int leftSum=0;
		int rightSum=0;
		for(int i=0;i<inputs.length;i++)
		{
			sum+=inputs[i];
		}
		for(int i=inputs.length-1;i>0;i--)
		{
			leftSum=sum-inputs[i];
			sum -=inputs[i];
			if(leftSum==rightSum)
			{
				equilibriumIndexList.add(i);
			}
			rightSum+=inputs[i];
		}
		for(int i=0;i<inputs.length;i++)
		{
			sum+=inputs[i];
		}
		return equilibriumIndexList;
	}
}
