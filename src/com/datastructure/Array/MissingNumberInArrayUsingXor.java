package com.datastructure.Array;

public class MissingNumberInArrayUsingXor {
	public static void main(String[] args) {
		Integer[] inputs = new Integer[] { 1, 2, 4, 6, 3, 7, 8 };
		int lMissingNumber = getMissingNumberUsingSum(inputs);
		System.out.println(lMissingNumber);
		lMissingNumber = getMissingNumberUsingXor(inputs);
		System.out.println(lMissingNumber);
	}

	private static int getMissingNumberUsingSum(Integer[] inputs) {
		// TODO Auto-generated method stub
		int max = 0;
		int sum = 0;
		for (int i = 0; i < inputs.length; i++) {
			max = Math.max(max, inputs[i]);
			sum += inputs[i];
		}
		int maxSum = (max * (max + 1)) / 2;
		return (maxSum - sum);
	}

	private static int getMissingNumberUsingXor(Integer[] inputs) {
		// TODO Auto-generated method stub
		int max = inputs[0];
		int lInputXor = inputs[0];
		for (int i = 1; i < inputs.length; i++) {
			max = Math.max(max, inputs[i]);
			lInputXor = lInputXor ^ inputs[i];
		}
		int lTotalXor = 0;
		for (int i = 1; i <= max; i++) {
			lTotalXor = lTotalXor ^ i;
		}
		return (lInputXor ^ lTotalXor);
	}
}
