package com.datastructure.Array;

public class CommonElementsInThreeSortedArrays {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer ar1[] = new Integer[]{1, 5, 10, 20, 40, 80};
		Integer ar2[] = new Integer[]{6, 7, 20, 80, 100};
		Integer ar3[] = new Integer[]{3, 4, 15, 20, 30, 70, 80, 120};
		getCommonElements(ar1,ar2,ar3);
	}

	private static void getCommonElements(Integer[] ar1, Integer[] ar2, Integer[] ar3) {
		// TODO Auto-generated method stub
		int firstIndex = 0;
		int secondIndex = 0;
		int thirdIndex = 0;
		while(firstIndex<ar1.length && 
				secondIndex <ar2.length  && 
				thirdIndex < ar3.length) {
			if(ar1[firstIndex]<ar2[secondIndex] && 
					ar1[firstIndex]<ar3[thirdIndex]){
				firstIndex++;
			}
			else if(ar2[secondIndex]<ar1[firstIndex] && 
					ar2[secondIndex]<ar3[thirdIndex]){
				secondIndex++;
			}
			else if(ar3[thirdIndex]<ar2[secondIndex] && 
					ar3[thirdIndex]<ar1[firstIndex]){
				thirdIndex++;
			}
			else {
				System.out.println(ar1[firstIndex]);
				System.out.println("common in"+firstIndex+","+secondIndex+","+thirdIndex);
				firstIndex++;
				secondIndex++;
				thirdIndex++;
			}
		}
	}

}
