package com.datastructure.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Diwakar Mishra
 *Given an unsorted array arr[0..n-1] of size n,
 * find the minimum length subarray arr[s..e] 
 * such that sorting this subarray makes the whole array sorted.
 * Examples:
 * 1) If the input array is [10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60], 
 * your program should be able to find that the subarray lies between the indexes 3 and 8.
 * 2) If the input array is [0, 1, 15, 25, 6, 7, 30, 40, 50], 
 * your program should be able to find that the subarray lies between the indexes 2 and 5.
 */
public class MinimumLengthUnsortedSubarray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer [] inputs = new Integer[]{0, 1, 15, 25, 6, 7,2, 30, 40, 50};
		List<Integer> startEnd=getMinimumSubArray(inputs);
		System.out.println(startEnd);
	}

	private static List<Integer> getMinimumSubArray(Integer[] inputs) {
		// TODO Auto-generated method stub
		List<Integer> minMax=new ArrayList<Integer>();
		int left=-1;
		int right =-1;
		
		for(int i=0;i<inputs.length;i++)
		{
			int j=inputs.length-1-i;
			if(right>0 && left>0)
			{
				break;
			}
			if(i!=0)
			{
				if(inputs[i]<inputs[i-1] && left<0)
				{
					left=i;
				}
			}
			if(j!=inputs.length-1)
			{
				if(inputs[j]>inputs[j+1] && right<0)
				{
					right =j+1;
				}
			}
			
		}
		int startIndex=left>right?right:left;		
		int endIndex=left<right?right:left;
		int min= Integer.MAX_VALUE;
		int max= Integer.MIN_VALUE;
		for(int i=startIndex;i<=endIndex;i++)
		{
		max=Math.max(max, inputs[i]);
		min=Math.min(min, inputs[i]);
		}
		int i=0;
		while(inputs[i]<min)
		{
			i++;
		}
		minMax.add(i);
		i=inputs.length-1;
		while(inputs[i]>max)
		{
			i--;
		}
		minMax.add(i);
		return minMax;
	}

}
