package com.datastructure.Array;

public class ArrayRotation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Integer[] inputArray = new Integer[]{1, 2, 3, 4, 5, 6, 7};
		int rotation=2;
		rotateArrayBy(inputArray,rotation);
	}

	private static void rotateArrayBy(Integer[] inputArray, int rotation) {
		// TODO Auto-generated method stub
		int temp = inputArray[rotation];
		for(int i=0;i<inputArray.length;i++)
		{
			inputArray[i+rotation]=inputArray[i];
			
		}
	}

}
