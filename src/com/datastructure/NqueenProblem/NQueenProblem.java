package com.datastructure.NqueenProblem;

public class NQueenProblem {
	public static void main(String[] args) {
		int n = 4;
		Position[] QueenPosition = new Position[n + 1];
		
		
		solveNQueenProblem(QueenPosition, 0, 0, n);
		printPosition(QueenPosition,n);
	}

	private static void printPosition(Position[] queenPosition, int n) {
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
			{
				if(i==queenPosition[i-1].getRow() && j== queenPosition[i-1].getColumn())
				{
					System.out.print("#\t");
				}
				else
				{
					System.out.print("0\t");
				}
			}
			System.out.println("");
		}
		
	}

	private static boolean solveNQueenProblem(Position[] queenPosition, int row, int column, int n) {
		boolean isSpaceAvailable = false;
		int i = 0;
		for(int j=1;j<=4;j++)
		{
			if(queenPosition[0]==null && row==0 && column==0)
			{
				Position point = new Position();
				point.setRow(1);
				point.setColumn(j);
				queenPosition[0] = point;
			}
			while (queenPosition[i] != null) {
				if (j != queenPosition[i].getColumn()
						&& queenPosition[i].getRow() + queenPosition[i].getColumn() != row + 1 + j
						&& row + 1 - j != queenPosition[i].getRow() - queenPosition[i].getColumn()) {
					
					isSpaceAvailable = true;
				}
				else
				{
					isSpaceAvailable = false;
					break;
				}
				i++;
			}
			i=0;
			if (isSpaceAvailable) {
				System.out.println(row + 1 + " , " + j);
				Position point = new Position();
				point.setRow(row + 1);
				point.setColumn(j);
				queenPosition[row] = point;
				if (row + 1==n || solveNQueenProblem(queenPosition, row + 1, column, n)) {
					return true;
				}
				else
				{
					queenPosition[row]=null;
				}
			}
			else if(j==4)
			{
				return false;
			}
		}
		
		
		return false;

	}

}

class Position {
	int row;

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	int column;
}