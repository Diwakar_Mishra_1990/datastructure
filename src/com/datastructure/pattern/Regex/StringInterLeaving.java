package com.datastructure.pattern.Regex;

public class StringInterLeaving {
public static void main(String[] args) {

	String inputString1="aab";
	String inputString2="axy";
	String testString="aaxaby";
	String[][] lDynamicArrayMap=createDynamicArrayMap(inputString1,inputString2,testString);
	processDynamicArray(lDynamicArrayMap);
}

private static void processDynamicArray(String[][] lDynamicArrayMap) {
	System.out.println("String Interleaving is");
	System.out.println(lDynamicArrayMap[lDynamicArrayMap.length-1][lDynamicArrayMap[lDynamicArrayMap.length-1].length-1]);
	
}

private static String[][] createDynamicArrayMap(String inputString1,
		String inputString2, String testString) {
	String[][] lDynamicArrayMap=new String[inputString2.length()+2][inputString1.length()+2];
	lDynamicArrayMap[0][1]="";
	lDynamicArrayMap[1][0]="";
	lDynamicArrayMap[1][1]="T";
	
	for(int i=2;i<lDynamicArrayMap[0].length;i++)
	{
		lDynamicArrayMap[0][i]=inputString1.charAt(i-2)+"";
	}
	for(int j=2;j<lDynamicArrayMap[0].length;j++)
	{
		lDynamicArrayMap[j][0]=inputString2.charAt(j-2)+"";
	}
	for(int i=2;i<lDynamicArrayMap[0].length;i++)
	{
		if((testString.charAt(i-2)+"").equals(inputString1.charAt(i-2)+""))
		{
			lDynamicArrayMap[1][i]="T";
		}
		else
		{
			lDynamicArrayMap[1][i]="F";
		}
	}
	for(int i=2;i<lDynamicArrayMap.length;i++)
	{
		if((testString.charAt(i-2)+"").equals(inputString2.charAt(i-2)+""))
		{
			lDynamicArrayMap[i][1]="T";
		}
		else
		{
			lDynamicArrayMap[i][1]="F";
		}
	}
	for(int i=2;i<lDynamicArrayMap.length;i++)
	{
		for(int j=2;j<lDynamicArrayMap[i].length;j++)
		{
			if(lDynamicArrayMap[0][j].equals((testString.charAt(((i-1)+(j-2)))+"").toString())
					&&
					lDynamicArrayMap[i][0].equals((testString.charAt(((i-1)+(j-2)))+"").toString())
					)
			{
				if(lDynamicArrayMap[i-1][j]==lDynamicArrayMap[i][j-1])
				{
					lDynamicArrayMap[i][j]=lDynamicArrayMap[i-1][j-1];
				}
			}
			else if(lDynamicArrayMap[0][j].equals((testString.charAt(((i-1)+(j-2)))+"").toString()))
			{
				lDynamicArrayMap[i][j]=lDynamicArrayMap[i][j-1];;
			}
			else if(lDynamicArrayMap[i][0].equals((testString.charAt(((i-1)+(j-2)))+"").toString()))
			{
				lDynamicArrayMap[i][j]=lDynamicArrayMap[i-1][j];;
			}
			else
			{
				lDynamicArrayMap[i][j]="F";
			}
		}
	}
	printArray(lDynamicArrayMap);
	return lDynamicArrayMap;
}

private static void printArray(String[][] lDynamicArrayMap) {
	// TODO Auto-generated method stub
	for (int i = 0; i < lDynamicArrayMap.length; i++) {
		for (int j = 0; j < lDynamicArrayMap[i].length; j++) {
			System.out.print(lDynamicArrayMap[i][j] + "\t");
		}
		System.out.println("");
	}
}

}
