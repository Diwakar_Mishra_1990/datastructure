package com.datastructure.pattern.Regex;

import java.util.ArrayList;
import java.util.List;

public class MinimumEditDistance {

	public static void main(String[] args) {
		String lString1 = "abcdef";
		String lString2 = "azced";

		/**
		 * saniTizePattern(lPattern); ********sf***sa=*sf*sa
		 */
		String[][] lMatchingArray = new String[lString2.length() + 2][lString1.length() + 2];
		findMinimumEditDistance(lString1, lString2, lMatchingArray);
		//processDyanamicArray(lMatchingArray);
	}

	private static void processDyanamicArray(String[][] lMatchingArray) {
		int i = lMatchingArray.length - 1;
		int j = lMatchingArray[0].length - 1;
		List<String> subSequenceList = new ArrayList<String>();
		while (!lMatchingArray[i][j].equals("0")) {
			if (!lMatchingArray[i][j].equals(lMatchingArray[i - 1][j])
					&& !lMatchingArray[i][j].equals(lMatchingArray[i][j - 1])) {
				subSequenceList.add(0, lMatchingArray[i][0]);
				i--;
				j--;
			} else if (lMatchingArray[i][j].equals(lMatchingArray[i - 1][j])
					&& !lMatchingArray[i][j].equals(lMatchingArray[i][j - 1])) {
				i--;
			} else {
				j--;
			}
		}
		System.out.println(subSequenceList);
	}

	/**
	 * INFO "?"<=========> Any One Character "*"<=========>0 or more sequence
	 */
	private static void findMinimumEditDistance(String lString1, String lString2, String[][] lMatchingArray) {
		lMatchingArray[0][1] = " ";
		lMatchingArray[1][0] = " ";
		lMatchingArray[1][1] = "0";
		for (int j = 2; j < lMatchingArray[0].length; j++) {
			lMatchingArray[0][j] = lString1.charAt(j - 2) + "";
			lMatchingArray[1][j] = j-1 + "";
		}
		for (int j = 2; j < lMatchingArray.length; j++) {
			lMatchingArray[j][0] = lString2.charAt(j - 2) + "";
			lMatchingArray[j][1] = j-1 + "";
		}
		for (int i = 2; i < lMatchingArray.length; i++) {
			for (int j = 2; j < lMatchingArray[i].length; j++) {
				if (!lMatchingArray[i][0].equals(lMatchingArray[0][j])) {
					lMatchingArray[i][j] = 1 + Math.min(Math.min(Integer.parseInt(lMatchingArray[i - 1][j]),
							Integer.parseInt(lMatchingArray[i][j - 1])), Integer.parseInt(lMatchingArray[i-1][j - 1])) + "";
				} else {
					lMatchingArray[i][j] = Integer.parseInt(lMatchingArray[i - 1][j - 1])+ "";
				}
			}
		}
		printArray(lMatchingArray);

	}

	private static void printArray(String[][] lMatchingArray) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lMatchingArray.length; i++) {
			for (int j = 0; j < lMatchingArray[i].length; j++) {

				System.out.print(lMatchingArray[i][j] + "\t");
			}
			System.out.println("");
		}
		System.out.println("");
	}
}
