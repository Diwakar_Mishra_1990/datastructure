package com.datastructure.pattern.Regex;

/**
 * INFO  
 * "?"<=========> Any One Character
 * "*"<=========>0 or more sequence
 */

/**
 * @author Diwakar Mishra
 *
 */
public class WildCharacterMatching {

	public static void main(String[] args) {
		String lPattern = "x?y*z";
		String lInputString = "xaylmz";

		/**
		 * saniTizePattern(lPattern); ********sf***sa=*sf*sa
		 */
		lPattern = saniTizePattern(lPattern);
		String[][] lMatchingArray = new String[lInputString.length() + 2][lPattern.length() + 2];
		findRegexMatch(lPattern, lInputString, lMatchingArray);
	}

	/**
	 * INFO  
	 * "?"<=========> Any One Character
	 * "*"<=========>0 or more sequence
	 */
	private static void findRegexMatch(String lPattern, String lInputString, String[][] lMatchingArray) {
		lMatchingArray[0][1] = " ";
		lMatchingArray[1][0] = " ";
		lMatchingArray[1][1] = "T";
		for (int j = 2; j < lMatchingArray[0].length; j++) {
			lMatchingArray[0][j] = lPattern.charAt(j - 2) + "";
		}
		for (int j = 2; j < lMatchingArray.length; j++) {
			lMatchingArray[j][0] = lInputString.charAt(j - 2) + "";
		}
		if ("*".equals(lPattern.charAt(0) + "")) {
			for (int j = 2; j < lMatchingArray.length; j++) {
				lMatchingArray[j][2] = "T";
			}
		}
		for (int j = 1; j < lMatchingArray.length; j++) {
			lMatchingArray[j][1] = "F";
		}
		for (int j = 1; j < lMatchingArray[1].length; j++) {
			lMatchingArray[1][j] = "F";
		}
		lMatchingArray[1][1] = "T";
		for (int i = 2; i < lMatchingArray.length; i++) {
			for (int j = 2; j < lMatchingArray[i].length; j++) {
				if ("?".equals(lMatchingArray[0][j])
						|| (lMatchingArray[i][0]).equals(lMatchingArray[0][j])) {
					lMatchingArray[i][j] = lMatchingArray[i - 1][j - 1];
				} else if ("*".equals(lMatchingArray[0][j])) {
					boolean rowBool = false;
					boolean colBool = false;
					boolean netBool = false;
					if (lMatchingArray[i - 1][j].equals("T")) {
						rowBool = true;
					}
					if (lMatchingArray[i][j - 1].equals("T")) {
						colBool = true;
					}
					netBool = colBool || rowBool;
					if (netBool) {
						lMatchingArray[i][j] = "T";
					} else {
						lMatchingArray[i][j] = "F";
					}

				}
				else
				{
					lMatchingArray[i][j] = "F";
				}
			}
		}
		printArray(lMatchingArray);
		if(lMatchingArray[lMatchingArray.length-1][lMatchingArray[lMatchingArray.length-1].length-1].equals("T"))
		{
			System.out.println("REGEX MATCHED");
		}
	}

	private static void printArray(String[][] lMatchingArray) {
		// TODO Auto-generated method stub
		for (int i = 0; i < lMatchingArray.length; i++) {
			for (int j = 0; j < lMatchingArray[i].length; j++) {
				
				System.out.print(lMatchingArray[i][j]+"\t");
			}
			System.out.println("");
		}
		System.out.println("");
	}

	/**
	 * Remove multiple occurence of *
	 * 
	 * @param lPattern
	 */
	private static String saniTizePattern(String lPattern) {
		String s = "";
		int lastStarFound = -1;
		for (int i = 0; i < lPattern.length(); i++) {

			if ("*".equals(lPattern.charAt(i) + "")) {
				if (lastStarFound != -1 && lastStarFound == i - 1) {
					lastStarFound = i;
					continue;
				}
				lastStarFound = i;
			}
			s += lPattern.charAt(i);
		}
		System.out.println(s);
		return s;
	}
}
